import java.io.*;
import java.net.*;
import javax.servlet.*;
import javax.servlet.http.*;
import org.jdom.*;
import org.jdom.input.*;
import org.jdom.output.*;

public class CloudRequest extends HttpServlet
{
	private static Namespace ns = Namespace.getNamespace("http://www.cs.au.dk/dWebTek/2011");
	
	private ServletContext context;
	private String shopFile;
	private Document shops;
	
	public void doGet(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException
	{
		/*
			Set up the cloud request
			The req-string can be any of the accepted requests accepted
			by the cloud.
			url is simply a URL constructed with the cloud-URI and the
			req-string concatenated.
		*/
		String req = "listShops";
		URL url = new URL("http://services.brics.dk/java/cloud/" + req);
		
		/*
			The SAXBuilder's build() method can build a JDOM Document
			object, and it accepts filenames as well as URIs. In this
			case, the URI is simply the cloud URL from before.
			The XMLOutputter object outputs an XML document using a
			Writer object, which in this case is a Writer object from
			the HttpServletResponse object (response).
			An XMLTransformer object can be used to transform the XML
			document using an XSLT stylesheet.
		*/
		SAXBuilder b = new SAXBuilder();
		PrintWriter writer = response.getWriter();
		try
		{
			Document shops = b.build(url);
			
			XMLOutputter out = new XMLOutputter();
			out.output(shops, writer);
		}
		catch (JDOMException e)
		{
			writer.write("Shit!");
		}
		writer.close();
	}
}