<%@ page import="java.io.*,javax.xml.transform.*,javax.xml.transform.stream.*" %>

<!--
	index.jsp
	
	Denne JSP-fil henter XML data fra cloud'en vha. linket i cloudRequest
	variablen.
	
	TransformerFactory og Transformer bruges til XSL transformationer, og
	File() _SKAL_ v�re en absolut sti, for at det hele virker.
	
	Det skulle ikke v�re noget st�rre problem at g�re dokumentet modul�rt,
	s� vi kan plotte forskellige request strings ind, og bruge forskellige
	XSL stylesheets afh�ngigt af requesten.
-->

<!--
	listItems.xsl

	XSL stylesheet XML data fra request "listItems?shopID=#"
	fra cloud'en.
	
	Der er nogle problemer med itemDescription - t�nker at XML schemaet
	skal linkes eller s�dan noget.
	
	Det viser sig at XSLT hurtigt bliver en smule forvirrende at l�se,
	s� det kan godt v�re, at vi skal bruge en anden approach. Dog er det
	langt mere l�sbart, end hvis man kun brugte servlets.
	
	JSP-dokumentet skulle gerne kunne opbygges meget modul�rt, s� vi kan
	have et XSL stylesheet for hver cloud request, hvilket kan medvirke
	til en langt h�jere l�sbarhed.
-->

<html>
	<head><title>JSP 2</title></head>
	
	<body>
		<%
			try
			{
				String cloudRequest = "http://services.brics.dk/java/cloud/listItems?shopID=12";
				TransformerFactory tf = TransformerFactory.newInstance();
				Transformer t = tf.newTransformer(new StreamSource(new File("C:\\Program Files (x86)\\Apache Software Foundation\\Tomcat 7.0\\webapps\\jsp2\\listItems.xsl")));				
				t.transform(new StreamSource(cloudRequest), new StreamResult(out));
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		%>
	</body>
</html>