<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:w="http://www.cs.au.dk/dWebTek/2011"
	xmlns="http://www.w3.org/1999/xhtml">
	
	<xsl:template match="w:items">
		<xsl:apply-templates select="w:item"/>
	</xsl:template>
	
	<xsl:template match="w:item">
		<h2>
			<xsl:apply-templates select="w:itemName"/>
		</h2>
		
		<table border="1">
			<tr>
				<td><img width="100" src="{w:itemURL}"/></td>
				<td>
					Price: <xsl:apply-templates select="w:itemPrice"/> kr.<br/>
					Stock: <xsl:apply-templates select="w:itemStock"/> stk.<br />
					<p>
						Description:<br />
						<xsl:apply-templates select="w:itemDescription"/>
					</p>
				</td>
			</tr>
		</table>
		<hr />
	</xsl:template>
	
	<xsl:template match="w:itemURL|w:itemName|w:itemPrice|w:itemStock|w:itemDescription">
		<xsl:value-of select="text()"/>
	</xsl:template>
	
</xsl:stylesheet>