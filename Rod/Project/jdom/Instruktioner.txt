Nyttige v�rkt�jer
XML well-formedness checker: 
   java -classpath jdom.jar:. CheckWellformed foo.xml
 
DTD validator: 
   java -classpath jdom.jar:. ValidateDTD foo.xml
 
XML Schema validator: 
   java -classpath jdom.jar:. ValidateXMLSchema foo.xml
 
XML Schema Quality Checker: 
   java -classpath sqc.jar com.ibm.sketch.util.SchemaQualityChecker foo.xsd
 
XPath Explorer: 
   java -jar xpe.jar
 
XSLT 2.0 processor: 
   java -classpath saxon.jar net.sf.saxon.Transform foo.xml bar.xsl
 
jar-filer til JSTL: jstl-api-1.2.jar , jstl-impl-1.2.jar (placeres i WEB-INF/lib) 
XPath evaluator: jaxen.jar (kan bruges med f.eks. JDOM)

Download de angivne jar/class-filer. P� Windows bruges ' ; ' i stedet for ' : ' i classpath'en.