#include <iostream>
#include <vector>
#include <cstdlib>
using namespace std;

// Simply calculate factorial using recurision.
int factorial(int n)
{
    if (n <= 1)
        return 1;
    else
        return n * factorial(n-1);
}

// Search the vector given in vec for the key, given by key
// The templating is just to make sure we support any given
// vector, the compiler will automatically choose the logic
// choice. -That is simply call it with your vector and key
// and the compiler will generate one for these.
template<typename T>
bool contains(const vector<T>& vec, T& key)
{
    for(int x=0; x<vec.size(); x++)
        if(key==vec[x])
            return true;
    return false;
}

int main()
{
    // vector of strings, that contain the elements in
    // the object, these will be the ones that is in the
    // permutations.
    vector<string> elements;
    elements.push_back("%w..shopKey;");
    elements.push_back("%w..itemID;");
    elements.push_back("%w..itemName;");
    elements.push_back("%w..itemPrice;");
    elements.push_back("%w..itemURL;");
    elements.push_back("%w..itemDescription;");

    // vector of string, that will contain all permutations
    // after running the while(1) loop. - if one likes, they
    // can ofc sort these into alfabetical order, since these
    // are in random order, due to the implementation of the 
    // permutator.
    vector<string> found;
    while(1)
    {
        // since we know there will be factorial(n) mutations
        // lets just run till we got this number of mutations. 
        if(factorial(elements.size())==found.size())
            break;
        
        // vector of strings holding elements that still needs
        // to be in permutation. - This is ofcourse initialised
        // to be equal to the elements vector, and during the
        // picking of elements, it will shrink till its empty. 
        vector<string> elements_del = elements;

        // the string created, holds the created permutation string
        string created = "";
        created+="(";
        
        // run the loop til the vector has the size of 1,
        // we choose to stop at one, because we're going to
        // add the last element ourselves, to avoid the loop
        // generating a comma after this last one as well.
        while(elements_del.size() != 1)
        {
            // generate a random number between 0->elements_del.size()
            // then add the element at this index to the created string
            // and finish by removing this element from the elements_del
            // vector.
            int r = (rand()%elements_del.size());
            created+=elements_del.at(r);
            created+=", ";
            elements_del.erase(elements_del.begin() + r);
        }
        // add the last element
        created+=elements_del.front();
        created+=") |";

        // if the newly created string, doesn't exist in
        // the found vector, then add it, otherwise keep
        // running.
        if(!contains(found, created))
            found.push_back(created);
    }

    // Simply print out all of the mutation to the screen
    for(int x=0; x<found.size(); x++)
        cout << found[x] << endl;

}
