import javax.xml.transform.stream.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.validation.*;
import javax.xml.*;
import org.xml.sax.SAXException;

import java.io.File;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


/**
 * A sample application which shows how to perform a
 * XML document validation.
 */
public class Validate
{
    private static String schema   = "cloud_edit.xsd";
    private static String input    = "mod1-2.xml";

    public static String parser1()
    {
        String validity = "Valid";
        try
        {
            String schemaLang = "http://www.w3.org/2001/XMLSchema";
            SchemaFactory factory = SchemaFactory.newInstance(schemaLang);
            StreamSource s = new StreamSource(schema);
            Schema schema = factory.newSchema(s);
       
            Validator validator = schema.newValidator();
            validator.validate(new StreamSource(input));
        }
        catch(SAXException ex)
        {
            validity = "Invalid: " + ex.getMessage();
        }
        catch (Exception ex) 
        {
            ex.printStackTrace();
        }
        return validity;
    }

    public static void main(String[] args) 
        throws Exception
    {
        System.out.println(parser1());
    }
}

