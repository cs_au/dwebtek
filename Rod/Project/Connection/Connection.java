import java.net.*;
import java.io.*;

public class Connection
{
    public static void main(String[] args)
    {
        //Get_Request_Test();
        Post_Request_Test();
    }

    public static void Get_Request_Test()
    {
        try
        {
            String req = "http://services.brics.dk/java/cloud/listShops";
            HttpURLConnection con = 
                (HttpURLConnection) (new URL(req)).openConnection();

            con.setRequestProperty("User-Agent", "IXWT");
            con.setInstanceFollowRedirects(false);
            con.setRequestMethod("GET");

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));

            String line;
            while((line = in.readLine()) != null)
            {
                System.out.println(line);
            }

        } 
        catch (IOException e) 
        {
            System.err.println(e); 
        }
    }

    public static void Post_Request_Test()
    {
        try 
        {
            String data =   "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"              +
                            "<w:login xmlns:w=\"http://www.cs.au.dk/dWebTek/2011\">"    +
                                "<w:customerName>"                                      +
                                    "Skeen"                                             +
                                "</w:customerName>"                                     +
                                "<w:customerPass>"                                      +
                                    "NINJA"                                             + 
                                "</w:customerPass>"                                     +
                            "</w:login>"                                                ;
            
            String req = "http://services.brics.dk/java/cloud/login";
            //String req = "http://localhost:8080/POST-Reply/POST";

            HttpURLConnection con = 
                (HttpURLConnection) (new URL(req)).openConnection();
            con.setRequestProperty("User-Agent", "IXWT");
            con.setInstanceFollowRedirects(false);
            con.setRequestMethod("POST");
            con.setDoOutput(true);
            con.setDoInput(true);
            
            OutputStreamWriter out = new OutputStreamWriter(con.getOutputStream());
            out.write(data);
            out.flush();
            out.close(); 
            
            con.connect();
            if(con.getResponseCode() == HttpURLConnection.HTTP_OK)
            {
                BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            
                String line;
                while((line = in.readLine()) != null)
                {
                    System.out.println(line);
                }
            }
            else
            {
                System.out.println("DID NOT GET TO RECIEVING!");
                System.out.println("RESPONSE: " + con.getResponseCode());
            }
            con.disconnect();
        } 
        catch (MalformedURLException e) 
        {
            e.printStackTrace();
        } 
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        } 
        catch (IOException e) 
        {
            e.printStackTrace();
        }
    }
}

