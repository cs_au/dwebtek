import java.net.*;
import java.io.*;

//SINGLETON OBJECT
public class cloud
{
    //requests skulle nok returnere et document eller tilsvarende
    //m�ske skulle der v�re en af funktion der tog dokument som data, istedet
    //for at tage en string.
    public static void main(String args[])
    {
        String data =       //"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"              +
                            "<w:login xmlns:w=\"http://www.cs.au.dk/dWebTek/2011\">"    +
                                "<w:customerName>"                                      +
                                    "Skeen"                                             +
                                "</w:customerName>"                                     +
                                "<w:customerPass>"                                      +
                                    "NINJAa"                                             +
                                "</w:customerPass>"                                     +
                            "</w:login>"                                                ;

        String data2 = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<w:createItem xmlns:w=\"http://www.cs.au.dk/dWebTek/2011\"><w:shopKey>C11BB6903BE16B14A57A1DF6</w:shopKey><w:itemName>Test</w:itemName></w:createItem>";
        //cloud.request("createItem", data2);
        cloud.request("login", data);
        /*
        cloud.request("listShops");
        cloud.request("listCustomers");
        */
    }
    
    // No data = GET
    public static void request(String cloud_req_url)
    {
        request(cloud_req_url, "");
    }
    
    // Data = POST
    public static void request(String cloud_req_url, String data)
    {
        //Possibly add a validation check
        try
        {
            HttpURLConnection con = (HttpURLConnection)
                (new URL(("http://services.brics.dk/java/cloud/"
                          + cloud_req_url))).openConnection();

            //con.setRequestProperty("User-Agent", "IXWT");
            //con.setInstanceFollowRedirects(false);
            //con.setDoInput(true);
            
            if(data.length() != 0)
            {
            //    con.setRequestMethod("POST");
                con.setDoOutput(true);

                OutputStreamWriter out = new OutputStreamWriter(con.getOutputStream());
                out.write(data);
                out.flush();
                out.close();
            }
            /*
            else
            {
                con.setRequestMethod("GET");
            }
            */
            //con.connect();
            if(con.getResponseCode() == HttpURLConnection.HTTP_OK)
            {
                BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            
                String line;
                while((line = in.readLine()) != null)
                {
                    System.out.println(line);
                }
            }
            else
            {
                System.out.println("HttoURLConnection Issue; ErrorCode:\t" + con.getResponseCode());
            }
            //con.disconnect();
        } 
        catch (IOException e) 
        {
            e.printStackTrace();
        }
    }
}
