// XPATH in Java with this parser/document type
//
// http://www.roseindia.net/tutorials/xPath/java-xpath.shtml
//

import java.io.File;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class XMLReader
{
    public static void main(String argv[])
    {
        try 
        {
            // listItems?shopID=ID type
            String str = "a.xml";

            Document doc = ((DocumentBuilderFactory.newInstance()).newDocumentBuilder()).parse(new File(str));
            doc.getDocumentElement().normalize();

            System.out.println(doc.getDocumentElement().getNodeName());
            NodeList nodeList = doc.getElementsByTagName("w:item");

            String[] tags = {"w:itemID", "w:itemName",
                             "w:itemURL",  "w:itemPrice",
                             "w:itemStock", "w:itemDescription"};
            for(int s=0; s<nodeList.getLength(); s++) 
            {
                System.out.println("<w:item>");
                Node currentNode = nodeList.item(s);
                if (currentNode.getNodeType() == Node.ELEMENT_NODE) 
                {
                    for(int x=0; x<tags.length; x++)
                    {
                        Element currentElement = (Element) currentNode;
                        NodeList currentElementList = currentElement.getElementsByTagName(tags[x]);
                        Element currentNmElement = (Element) currentElementList.item(0);
                        NodeList currentNmElementList = currentNmElement.getChildNodes();
                        System.out.println("\t" + "<" + tags[x] + ">");
                        System.out.println("\t\t" + ((Node) currentNmElementList.item(0)).getNodeValue());
                        System.out.println("\t" + "<" + tags[x] + "/>");
                    }
                }
                System.out.println("<w:item/>");
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
