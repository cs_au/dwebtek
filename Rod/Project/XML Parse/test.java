import java.io.*;
import org.w3c.dom.*;
import javax.xml.xpath.*;
import javax.xml.parsers.*;
import javax.xml.namespace.*; 
import java.util.*;
import java.net.*;
import java.io.*;

public class test
{
    public static List<String> getShops(Document d)
        throws Exception
    {
        List<String> shopnames = new ArrayList<String>();

        XPath xpath = XPathFactory.newInstance().newXPath();
        xpath.setNamespaceContext(new NamespaceContext()
        {
            public String getNamespaceURI(String prefix)
            {
                if (prefix.equals("w"))
                    return "http://www.cs.au.dk/dWebTek/2011";
                else
                    return null;
                    //return XMLConstants.NULL_NS_URI;
            }

            public String getPrefix(String namespace)
            {
                if (namespace.equals("http://www.cs.au.dk/dWebTek/2011"))
                    return "w";
                else
                    return null;
            }

            public Iterator getPrefixes(String namespace)
            {
                return null;
            }
        });
        
        XPathExpression expr = xpath.compile("/descendant::w:shops/descendant::w:shop/descendant::w:shopURL");
        NodeList nodes = (NodeList) expr.evaluate(d, XPathConstants.NODESET);
        
        for(int x=0; x<nodes.getLength(); x++)
            shopnames.add(nodes.item(x).getFirstChild().getNodeValue());
        return shopnames;
    }


    public static void main(String args[])
        throws Exception
    {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        Document doc = dbf.newDocumentBuilder().parse(new File("shops.xml"));
        doc.getDocumentElement().normalize();

        List<String> names = null;
        names = getShops(doc);

        for(String s : names)
        {
            System.out.println(s);
        }
    }
}


