import java.io.*;
import org.jdom.*;
import org.jdom.input.*;
import org.jdom.output.XMLOutputter;

/* compile with
javac -classpath jdom.jar;. Validate.java
*/
public class Validate
{   
    
    public static void main(String[] args)
    {
        //1 ok
        validateXML(new File(args[0]));
        
        //2 ok
        // validateXML(new File(args[0]));
        
        //3 -ok
        validateXML(
            new org.xml.sax.InputSource(
                new StringReader(
                    (new XMLOutputter()).outputString(
                        doc(args[0])
                    )
                )
            )
        );
        
        System.out.println("_");
        System.out.println(new XMLOutputter().outputString(doc(args[0])));
        System.out.println("_");
        // String filename = "";
        // Document docy = doc(filename);
        // OutputStream out = new PipedOutputStream(PipedInputStream snk)
        
        // new XMLOutputter()).output(
                // docy,
                // java.io.OutputStream out)
    }    
    
    private static final Namespace w = Namespace.getNamespace("http://www.cs.au.dk/dWebTek/2011");
    private static final String exampleInteger = "1";
    
    // for later tests (use filter to acces itemID)
    // private static Filter f = new ElementFilter("itemID", w);
    
    // dynamically building document
    private static Document doc(String filename)
    {
        Document res = null;
        try
        {
            SAXBuilder b = new SAXBuilder();
                
            try
            {
                res = b.build(new File(filename));
                res.getRootElement().getChild("itemID", w).setText(exampleInteger);
                res.getRootElement().getChild("itemPrice", w).setText(exampleInteger);
            }
            catch(JDOMParseException e)
            {
                e.getMessage();
            }
            
        }
        catch(Exception e)
        {
            System.err.println(e);
        }
        
        return res;
    }
    
    // Validates the xmlFile
    // use new File(pathToFile) or a Reader
    private static void validateXML(org.xml.sax.InputSource in)
    {
        try
        {
            SAXBuilder b = new SAXBuilder();
            
            //validation should occur.
            b.setValidation(true); 
            
            // Allow validation against XML Schema descriptions (referenced in the XML-file).
            b.setProperty(
                "http://java.sun.com/xml/jaxp/properties/schemaLanguage",
                "http://www.w3.org/2001/XMLSchema");
            
            // String for response in cmd
            String msg = "Validation came through fine!";
            
            try
            {
                Document d = b.build(in);
            }
            catch(JDOMParseException e)
            {
                msg = e.getMessage();
            }
            System.out.println(msg);
        }
        catch(Exception e)
        {
            System.err.println(e);
        }
    }
    private static void validateXML(File xmlFile)
    {
        try
        {
            SAXBuilder b = new SAXBuilder();
            
            //validation should occur.
            b.setValidation(true); 
            
            // Allow validation against XML Schema descriptions (referenced in the XML-file).
            b.setProperty(
                "http://java.sun.com/xml/jaxp/properties/schemaLanguage",
                "http://www.w3.org/2001/XMLSchema");
            
            // String for response in cmd
            String msg = "Validation came through fine!";
            
            try
            {
                Document d = b.build(xmlFile);
            }
            catch(JDOMParseException e)
            {
                msg = e.getMessage();
            }
            System.out.println(msg);
        }
        catch(Exception e)
        {
            System.err.println(e);
        }
    }
}