// kilde: http://wiki.apache.org/tomcat/HowTo#How_do_I_check_whether_Tomcat_is_UP_or_DOWN.3F_There_is_no_status_command

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

/**
 * Check to see if Tomcat is UP/DOWN.
 *
 * This parses the server.xml file for the Tomcat admin port and see if
 * we can connect to it. If we can, then the Tomcat is UP otherwise it
 * is DOWN
 *
 * It is invoked as follows:
 *    java -Dcatalina.base=c:/tomcat-6.0.10 CatalinaStatus
 *
 * It can also (optionally) shutdown the Tomcat by adding the shutdown
 * command line parameter as follows:
 *
 *    java -Dcatalina.base=c:/tomcat-6.0.10 CatalinaStatus shutdown
 *
 * @author Shiraz Kanga <skanga at yahoo.com>
 */
public class CatalinaStatus
{
  /**
   * Pathname to the server configuration file.
   */
  protected static String configFile = "conf/server.xml";
  protected static String serverShutdown;
  protected static int serverPort;

  /**
   * The application main program.
   *
   * @param args Command line arguments
   */
  public static void main (String args[])
  {
    Document configDom = getXmlDom (configFile ());
    parseDocument (configDom);
    // System.out.println ("Catalina.serverPort: " + serverPort);
    // System.out.println ("Catalina.serverShutdown: " + serverShutdown);

    // Stop the existing server
    try
    {
      Socket localSocket = new Socket ("127.0.0.1", serverPort);
      System.err.println ("Server status:  UP");
      if ((args.length > 0) && (args[0].equalsIgnoreCase ("shutdown")))
      {
        System.out.println ("Tomcat shutdown initiated" );
        doShutdown (localSocket);
      }

      localSocket.close ();
    }
    catch (IOException e)
    {
      System.err.println ("Server status:  DOWN");
      System.exit(1);
    }
  }

  /**
   * Return a File object representing our configuration file.
   */
  protected static File configFile ()
  {
    File confFile = new File (configFile);
    if (!confFile.isAbsolute())
      confFile = new File (System.getProperty ("catalina.base"), configFile);
    return (confFile);
  }

  /**
   * Parses an XML file and returns a DOM document.
   */
  public static Document getXmlDom (File fileName)
  {
    try
    {
      // Create a builder factory
      DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance ();

      // Create the builder and parse the file
      Document doc = factory.newDocumentBuilder ().parse (fileName);
      return doc;
    }
    catch (SAXException e)
    {
      // A parsing error occurred; the xml input is not valid
      e.printStackTrace ();
    }
    catch (ParserConfigurationException e)
    {
      e.printStackTrace ();
    }
    catch (IOException e)
    {
      e.printStackTrace ();
    }
    return null;
  }

  /**
   * Extract the server port & shutdown command from the DOM
   */
  private static void parseDocument (Document configDom)
  {
    //get the root element which is Server Eg: <Server port="8005" shutdown="SHUTDOWN">

    Element docEle = configDom.getDocumentElement ();
    serverPort = Integer.parseInt (docEle.getAttribute ("port"));
    serverShutdown = docEle.getAttribute ("shutdown");
  }

  /**
   * Send the shutdown command to the server
   */
  private static void doShutdown (Socket localSocket)
  {
    try
    {
      OutputStream outStream = localSocket.getOutputStream ();

      for (int i = 0; i < serverShutdown.length (); i++)
        outStream.write (serverShutdown.charAt (i));
      outStream.flush ();
      outStream.close ();
    }
    catch (IOException e)
    {
      System.out.println ("ERROR: I/O Exception during server shutdown.");
      e.printStackTrace ();
    }
  }
}