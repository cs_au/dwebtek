<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:w="http://www.cs.au.dk/dWebTek/2011" version="1.0">
	<xsl:template match="/">
		<form method="get" action="redirect">
			<select name="selected">
				<xsl:for-each select="w:shops/w:shop">
					<xsl:sort select="w:shopName"/>
					<xsl:variable name="linkage">
						<xsl:value-of select="w:shopURL"/>
					</xsl:variable>
					<option value="{$linkage}">
						<xsl:value-of select="w:shopName"/>
					</option>
				</xsl:for-each>
			</select>
			<input type="submit" name="submit" value="Go!"/>
		</form>
	</xsl:template>
</xsl:stylesheet>
