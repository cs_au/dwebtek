<%
	RequestDispatcher itemsServletdispatcher = application.getRequestDispatcher("/itemsServlet"); 
	itemsServletdispatcher.include(request,response);
%>

<!-- This is included in the body element of the index.jsp-->
<!-- This page requests and renders information about items through the itemsServlet-->
<!-- It also includes buying link if User is logged in-->
<h2>Items for sale</h2>
<!-- If logged in -->

<% if(session.getAttribute("customerName")!=null) { %>   
    ${itemsTableLoggedIn}
<!-- else if not logged in -->
<% } else { %>  
    ${itemsTable}
<% } %>  

