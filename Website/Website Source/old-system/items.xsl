<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:w="http://www.cs.au.dk/dWebTek/2011" version="1.0">
	<xsl:template match="/">
		<table border="1">
			<tr>
				<th>Image</th>
				<!-- <th>itemID</th>      not relevant data-->
				<th>Name</th>
				<th>Price</th>
				<th>Stock</th>
				<th>Description</th>
			</tr>
			<xsl:for-each select="w:items/w:item">
				<xsl:sort select="w:itemName"/>
				<tr>
					<td>
						<xsl:variable name="linkage">
							<xsl:value-of select="w:itemURL"/>
						</xsl:variable>
						<img src="{$linkage}" alt="LOL"/>
					</td>
					<!-- <td><xsl:value-of select="w:itemID"/></td> -->
					<td>
						<xsl:value-of select="w:itemName"/>
					</td>
					<td>
						<xsl:value-of select="w:itemPrice"/>
					</td>
					<td>
						<xsl:value-of select="w:itemStock"/>
					</td>
					<td>
						<xsl:value-of select="w:itemDescription"/>
					</td>
				</tr>
			</xsl:for-each>
		</table>
	</xsl:template>
</xsl:stylesheet>
