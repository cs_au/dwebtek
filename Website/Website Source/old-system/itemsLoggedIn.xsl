<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:w="http://www.cs.au.dk/dWebTek/2011" version="1.0">
	<xsl:template match="/">
		<table border="1">
			<tr>
				<th>itemID</th>
				<th>itemName</th>
				<th>itemURL</th>
				<th>itemPrice</th>
				<th>itemStock</th>
				<th>itemDescription</th>
				<th>NumItems and Buy</th>
			</tr>
			<xsl:for-each select="w:items/w:item">
				<xsl:sort select="w:itemName"/>
				<tr>
					<td>
						<xsl:variable name="linkage">
							<xsl:value-of select="w:itemURL"/>
						</xsl:variable>
						<img src="{$linkage}" alt="LOL"/>
					</td>
					<xsl:variable name="itemID">
						<xsl:value-of select="w:itemID"/>
					</xsl:variable>
					<xsl:variable name="itemName">
						<xsl:value-of select="w:itemName"/>
					</xsl:variable>
					<xsl:variable name="itemStock">
						<xsl:value-of select="w:itemStock"/>
					</xsl:variable>
					<td>
						<xsl:value-of select="w:itemID"/>
					</td>
					<td>
						<xsl:value-of select="w:itemName"/>
					</td>
					<td>
						<xsl:value-of select="w:itemPrice"/>
					</td>
					<td>
						<xsl:value-of select="w:itemStock"/>
					</td>
					<td>
						<xsl:value-of select="w:itemDescription"/>
					</td>
					<td>
						<form method="post" action="addServlet">
							<label>Number Of Items</label>
							<input name="itemID" type="hidden" value="{$itemID}"/>
							<input name="itemName" type="hidden" value="{$itemName}"/>
							<input name="itemStock" type="hidden" value="{$itemStock}"/>
							<input name="saleAmount" type="text"/>
							<br/>
							<input type="submit" name="submit" value="Add"/>
						</form>
					</td>
				</tr>
			</xsl:for-each>
		</table>
	</xsl:template>
</xsl:stylesheet>
