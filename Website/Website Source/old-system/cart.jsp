<%
	RequestDispatcher shoppingCartServletdispatcher = application.getRequestDispatcher("/shoppingCartServlet"); 
	shoppingCartServletdispatcher.include(request,response);
%>

<!-- This is included in the body element of the index.jsp-->
<!-- This page requests and renders information about items through the itemsServlet-->
<!-- It also includes buying link if User is logged in-->

<!-- If logged in -->
<% 
if(session.getAttribute("customerName")!=null)
{ 
%>   
<h2>ShoppingCart</h2>
    ${ShoppingCart}
<%
}
%>  

