<!-- This file is the main document. It includes header.jsp, login.jsp and items.jsp -->
<html>
    <head>
        <title>HorsePower Webshop</title>
    </head>
    <body>
        <%@ include file="header.jsp" %>
        <%@ include file="shops.jsp" %>
        <%
        String error = request.getParameter("error");
        if(error!=null)
        {
            out.print("<b><font color=red>" + error + "</font></b>");
        }
        %>
        <%@ include file="login.jsp" %>
        <%@ include file="cart.jsp" %>
        <%@ include file="items.jsp" %>
        Goto <a href="/HorsePower/backstore/backstore.jsp">backstore</a>!
    </body>
</html>

