import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

import skeen.pair;

public class ShoppingCartServlet extends HttpServlet 
{
    public void doGet(HttpServletRequest request, 
            HttpServletResponse response)
        throws IOException, ServletException 
    {
        HttpSession session = request.getSession();
        Map cart = (Map)session.getAttribute("cart");
        if (cart == null) 
        {
            cart = new TreeMap();
            session.setAttribute("cart", cart);
        }
        
        String xhtml = "Your shopping cart is empty.";
        if(!cart.isEmpty())
        {
            xhtml = "";
            xhtml = xhtml.concat("Your shopping cart now contains:<p>" + "<table border=1><tr><th>Item<th>Amount");
            Iterator i = cart.entrySet().iterator();
            while (i.hasNext()) 
            {
                Map.Entry me = (Map.Entry)i.next();
                xhtml = xhtml.concat("<tr><td>" + me.getKey() + "<td align=right>" + ((pair) me.getValue()).getSecond());
            }
            xhtml = xhtml.concat("</table><p>");
            xhtml = xhtml.concat("<form method=post action=\"" + response.encodeURL("buyServlet") + "\">" + "<input type=submit name=submit value=\"Checkout\">" + "</form>");
            xhtml = xhtml.concat("<form method=post action=\"" + response.encodeURL("clearServlet") + "\">" + "<input type=submit name=submit value=\"Reset\">" + "</form>");
        }
        session.setAttribute("ShoppingCart", xhtml);
    }

    public void doPost(HttpServletRequest request, 
            HttpServletResponse response)
        throws IOException, ServletException 
    {
        doGet(request, response);
    }
}
