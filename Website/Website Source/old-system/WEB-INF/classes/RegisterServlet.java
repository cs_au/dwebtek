import java.io.*;
import java.net.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import org.jdom.*;
import org.jdom.input.*;
import org.jdom.output.*;
import org.jdom.transform.*;

import skeen.*;

public class RegisterServlet extends HttpServlet
{
    public void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException
    {
        String shopKey = getServletContext().getInitParameter("shopKey");

        HttpSession session = request.getSession();
        String contextPath = request.getContextPath();
		Namespace w = Namespace.getNamespace("http://www.cs.au.dk/dWebTek/2011");
		Document createCustomerRequest = null;

        String customerName = request.getParameter("customerName");
        String customerPass = request.getParameter("customerPass");
        String customerPassRepeat = request.getParameter("customerPassRepeat");
        
        //sbr: shouldn't we use the customerName xml-specification from cloud.xsd?
        //sbr: Hvordan haandterer vi 'user already exists' i createcustomerResponse fra cloud.xsd?
        if(customerName==null || customerPass==null || customerPassRepeat==null)
        {
             response.sendRedirect(response.encodeRedirectURL(contextPath + "?error=" + URLEncoder.encode("Missing Argument", "UTF-8")));
             return;
        }
        else if(!customerPassRepeat.equals(customerPass))
        {
            response.sendRedirect(response.encodeRedirectURL(contextPath + "/register?error=" + URLEncoder.encode("The passwords are not identical!", "UTF-8")));
            return;
        }
        else if(customerPass.length() < 3)
        {
            response.sendRedirect(response.encodeRedirectURL(contextPath + "/register?error=" + URLEncoder.encode("Password is too short!", "UTF-8")));
            return;
        }
        else if(customerName.length() < 2)
        {
            response.sendRedirect(response.encodeRedirectURL(contextPath + "/register?error=" + URLEncoder.encode("Username is too short!", "UTF-8")));
            return;
        }

        try
        {
            /* Build a new Document object from the loginRequest.xml file.
			   Set the object's customerName and customerPass element values
			   to the ones from the HTTP request. */
            createCustomerRequest = new SAXBuilder().build(new File(getServletContext().getRealPath("createCustomerRequest.xml")));
			createCustomerRequest.getRootElement().getChild("shopKey", w).setText(shopKey);
			createCustomerRequest.getRootElement().getChild("customerName", w).setText(customerName);
			createCustomerRequest.getRootElement().getChild("customerPass", w).setText(customerPass);
        }
        catch(Exception e)
        {
			PrintWriter out = response.getWriter();
			out.write("FUCK!");
			out.close();
        }

        Document createCustomerResponse = null;
        try
        {
            createCustomerResponse = cloud.request("createCustomer", createCustomerRequest);
        }
        catch (BadUrlResponse e)
        {
            if (e.getResponseCode() == HttpURLConnection.HTTP_BAD_REQUEST )
            {
                response.sendRedirect(response.encodeRedirectURL(contextPath + "/register?error=" + URLEncoder.encode("Unable to create user!", "UTF-8")));
                return;
            }
            else
            {
                String recv = getServletContext().getInitParameter("email-devs");
                String[] to = recv.split(" ");
                mail.send(to, "", "Bad response codeerror", "Thrown from: RegisterServlet. Value: " + e.getResponseCode());
            }
        }

        // User was created!
        if(createCustomerResponse!=null)
        {
            session.setAttribute("customerName", customerName);
            response.sendRedirect(response.encodeRedirectURL(contextPath));
            return;
        }
        else
        {
            String recv = getServletContext().getInitParameter("email-devs");
            String[] to = recv.split(" ");
            mail.send(to, "", "createCustomerResponse", "Thrown from: RegisterServlet. createCustomerResponse is null");            
            response.sendRedirect(response.encodeRedirectURL(contextPath + "/register?error=" + URLEncoder.encode("Unable to create user!", "UTF-8")));
            return;
        }

    }
    
    // force post request
    public void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException
    {
        doPost(request, response);
    }
}


