import java.io.*;
import java.net.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import org.jdom.*;
import org.jdom.input.*;
import org.jdom.output.*;
import org.jdom.transform.*;

import skeen.*;

public class BuyServlet extends HttpServlet
{
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String shopKey = getServletContext().getInitParameter("shopKey");

        HttpSession session = request.getSession();
        String contextPath = request.getContextPath();
		Namespace w = Namespace.getNamespace("http://www.cs.au.dk/dWebTek/2011");
		Document sellItemsRequest = null;
        String customerID = (String) session.getAttribute("customerID");

        if(customerID==null)
        {
             response.sendRedirect(response.encodeRedirectURL(contextPath + "?error=" + URLEncoder.encode("Missing Argument", "UTF-8")));
             return;
        }
        
        Map cart = (Map)session.getAttribute("cart");
        if(!cart.isEmpty())
        {
            Iterator i = cart.entrySet().iterator();
            while (i.hasNext()) 
            {
                Map.Entry me = (Map.Entry)i.next();
                try
                {
                    /* Build a new Document object from the sellItemsRequest.xml file. */
                    sellItemsRequest = new SAXBuilder().build(new File(getServletContext().getRealPath("sellItemsRequest.xml")));
                    sellItemsRequest.getRootElement().getChild("shopKey", w).setText(shopKey);
                    sellItemsRequest.getRootElement().getChild("itemID", w).setText("" + ((pair) me.getValue()).getFirst());
                    sellItemsRequest.getRootElement().getChild("customerID", w).setText(customerID);
                    sellItemsRequest.getRootElement().getChild("saleAmount", w).setText("" + ((pair) me.getValue()).getSecond());
                }
                catch(Exception e)
                {
                    PrintWriter out = response.getWriter();
                    out.write("FUCK!");
                    out.close();
                }

                Document sellItemResponse = null;
                try
                {
                    sellItemResponse = cloud.request("sellItems", sellItemsRequest);
                }
                catch(BadUrlResponse e)
                {
                    if (e.getResponseCode() == HttpURLConnection.HTTP_BAD_REQUEST )
                    {
                        //GOTO ITEM NOT SOLD
                        //response.sendRedirect(response.encodeRedirectURL(contextPath + "?error=" + URLEncoder.encode("Bad Request: YOU CANT HAVE THE REQUESTED ITEM", "UTF-8")));
                        //return;
                    }
                    else
                    {
                        String recv = getServletContext().getInitParameter("email-devs");
                        String[] to = recv.split(" ");
                        mail.send(to, "", "Bad response codeerror", "Thrown from: BuyServlet. Value: " + e.getResponseCode());
                    }
                }


                if(sellItemResponse!=null)
                {
                    // User could log in!
                    //response.sendRedirect(response.encodeRedirectURL(contextPath + "/itemSold"));
                    //return;
                }
                else
                {
                    String recv = getServletContext().getInitParameter("email-devs");
                    String[] to = recv.split(" ");
                    mail.send(to, "", "SellItemResponse", "Thrown from: BuyServlet. SellItemResponse is null");
                    //return;
                }
            }
            session.setAttribute("cart", new TreeMap());
            response.sendRedirect(response.encodeRedirectURL(contextPath));
        }
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        doPost(request, response);
    }
}

