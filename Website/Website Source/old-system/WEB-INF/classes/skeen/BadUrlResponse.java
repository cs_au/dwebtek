package skeen;

public class BadUrlResponse extends Exception
{
    private int responseCode = 0;
    
    public BadUrlResponse(String responseCode)
    {
        super(responseCode);
        try
        {
            this.responseCode = Integer.parseInt(responseCode);
        }
        catch (Exception e)
        { 
            // Crash java VM
            Object[] o = null;
            while (true)
            {
                o = new Object[] {o};
            }
        }
    }
    
    public int getResponseCode()
    {
        return responseCode;
    }
}