package skeen;

public class pair<A, B>
{
    private A first;
    private B second;

    public pair(A first, B second)
    {
        super();
        this.first = first;
        this.second = second;
    }

    public int hashCode()
    {
        int hashFirst = first != null ? first.hashCode() : 0;
        int hashSecond = second != null ? second.hashCode() : 0;

        return (hashFirst + hashSecond) * hashSecond + hashFirst;
    }

    public boolean equals(Object other) 
    {
        if (other instanceof pair) 
        {
            pair otherpair = (pair) other;
            return 
                ((  this.first == otherpair.first ||
                    ( this.first != null && otherpair.first != null &&
                      this.first.equals(otherpair.first))) &&
                 (      this.second == otherpair.second ||
                        ( this.second != null && otherpair.second != null &&
                          this.second.equals(otherpair.second))) );
        }
        return false;
    }

    public String toString()
    { 
        return "(" + first + ", " + second + ")"; 
    }

    public A getFirst() 
    {
        return first;
    }

    public void setFirst(A first) 
    {
        this.first = first;
    }

    public B getSecond() 
    {
        return second;
    }

    public void setSecond(B second) 
    {
        this.second = second;
    }
}

