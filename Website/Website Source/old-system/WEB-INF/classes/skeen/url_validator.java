package skeen;

import java.net.*;
import java.io.*;

public class url_validator
{
    public static boolean exists(String URLName)
    {
        try 
        {
            HttpURLConnection.setFollowRedirects(true);
            //HttpURLConnection.setInstanceFollowRedirects(true);
            HttpURLConnection con =
                (HttpURLConnection) new URL(URLName).openConnection();
            con.setRequestMethod("HEAD");
            return (con.getResponseCode() == HttpURLConnection.HTTP_OK);
        }
        catch (Exception e) 
        {
            e.printStackTrace();
            return false;
        }
    }
}
