package skeen;

import javax.mail.*;
import javax.mail.internet.*;
import java.util.*;

public class mail
{
    private static String login_host = "smtp.gmail.com";
    private static String login_user = "daimler.servlet@gmail.com";
    private static String login_pass = "NINJA-TESTER";

    public static boolean send(String to, String from, String subject, String contents)
    {
        String[] strarray = new String[1];
        strarray[0] = to;
        return send(strarray, from, subject, contents);
    }

    public static boolean send(String[] to, String from, String subject, String contents)
    {
        try
        {
            Properties props = System.getProperties();
            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.host", login_host);
            props.put("mail.smtp.user", login_user);
            props.put("mail.smtp.password", login_pass);
            props.put("mail.smtp.port", "587");
            props.put("mail.smtp.auth", "true");

            Session session = Session.getDefaultInstance(props, null);
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(from));

            InternetAddress[] toAddress = new InternetAddress[to.length];

            // To get the array of addresses
            for( int i=0; i < to.length; i++ )
            {
                toAddress[i] = new InternetAddress(to[i]);
            }

            for( int i=0; i < toAddress.length; i++)
            {
                message.addRecipient(Message.RecipientType.TO, toAddress[i]);
            }
            message.setSubject(subject);
            message.setText(contents);
            Transport transport = session.getTransport("smtp");
            transport.connect(login_host, login_user, login_pass);
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
            return true;
        }
        catch(Exception e)
        {
            return false;
        }
    }
}

