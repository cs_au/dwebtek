package skeen;

import java.util.*;

import skeen.ChartSizeException;

/* Example usage
        try
        {
            List<pair<Integer, String>> data = new ArrayList<pair<Integer, String>>();
            data.add(new pair(60, "HELLO"));
            data.add(new pair(20, "WORLD"));
            data.add(new pair(20, "MOR"));

            String rofl = "";
            rofl = rofl.concat("<img src=\"");
            rofl = rofl.concat(chart.generate("p3", 250, 100, data));
            rofl = rofl.concat("\" />");
            session.setAttribute("chart", rofl);
        }
        catch(Exception e)
        {
            session.setAttribute("chart", "failure");
        }
*/

public class chart
{
    //private static int id = 0;

    public static String generate(String chartType, int width, int height, List<pair<Integer, String>> data)
        throws ChartSizeException
    {
        //STRING
        String xhtml = "http://";
        
        //ID
        /*
        xhtml = xhtml.concat(id + ".");
        id++;
        if(id==10)
            id = 0;
        */

        //STRING
        xhtml = xhtml.concat("chart.googleapis.com/chart?cht=");

        //CHART_TYPE
        xhtml = xhtml.concat(chartType + "&");

        //SIZE
        if(width>1000)
            throw new ChartSizeException("Width can't exceed 1000");
        else if(height>1000)
            throw new ChartSizeException("height can't exceed 1000");
        else if((width*height)>300000)
            throw new ChartSizeException("Width x Height can't exceed 300,000");
        xhtml = xhtml.concat("chs=" + width + "x" + height + "&");

        //DATA
        xhtml = xhtml.concat("chd=t:");
        for(pair p : data)
        {
            xhtml = xhtml.concat(p.getFirst() + ",");
        }
        xhtml = xhtml.substring(0, xhtml.length()-1); 
        xhtml = xhtml.concat("&");

        //LABELS
        xhtml = xhtml.concat("chl=");
        for(pair p : data)
        {
            xhtml = xhtml.concat(p.getSecond() + "|");
        }
        xhtml = xhtml.substring(0, xhtml.length()-1);
        
        //RETURN IT
        return xhtml;
    }
}
