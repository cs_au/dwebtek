package skeen;
import skeen.BadUrlResponse;
import java.net.*;
import java.io.*;
import org.jdom.*;
import org.jdom.transform.*;
import org.jdom.input.*;
import org.jdom.output.*;

public class cloud
{
    // No data = GET
    public static Document request(String cloud_req_url)
        throws BadUrlResponse
    {
        return request(cloud_req_url, null);
    }
    
    // Data = POST
    public static Document request(String cloud_req_url, Document data)
        throws BadUrlResponse
    {
        Document doc = null;
        try
        {
            HttpURLConnection con = (HttpURLConnection)
                (new URL(("http://services.brics.dk/java/cloud/"
                          + cloud_req_url))).openConnection();
            
            if(data != null)
            {
                con.setDoOutput(true);

                XMLOutputter out = new XMLOutputter();
				out.output(data, con.getOutputStream());
            }
            
            if(con.getResponseCode() == HttpURLConnection.HTTP_OK)
            {
                SAXBuilder b = new SAXBuilder();
                doc = b.build(con.getInputStream());
            }
            else
                throw new BadUrlResponse("" + con.getResponseCode());
                //System.out.println("HttoURLConnection Issue; ErrorCode:\t" + con.getResponseCode());
        } 
        catch(Exception e) 
        {
            e.printStackTrace();
        }
        return doc;
    }
}
