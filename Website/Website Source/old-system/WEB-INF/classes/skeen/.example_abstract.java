import libs

public class horsePowerServlet extends HttpServlet
{
    private final static String shopKey = getServletContext().getInitParameter("shopKey");
    private final static Namespace w = Namespace.getNamespace("http://www.cs.au.dk/dWebTek/2011");
    
    private final HttpSession session;
    private final String contextPath;
    private Document dataRequest;
    

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        doPost(request, response);
    }
    
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        
    }
    
    private void errorRedirect(String errorMsg)
    {
        response.sendRedirect(response.encodeRedirectURL(contextPath + "?error=" + URLEncoder.encode("errorMsg", "UTF-8")));
    }
}