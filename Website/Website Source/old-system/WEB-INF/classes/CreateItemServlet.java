import java.io.*;
import java.net.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import org.jdom.*;
import org.jdom.input.*;
import org.jdom.output.*;
import org.jdom.transform.*;

import skeen.*;

public class CreateItemServlet extends HttpServlet
{
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		doPost(request, response);
	}
    
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
		String shopKey = getServletContext().getInitParameter("shopKey");
		Namespace w = Namespace.getNamespace("http://www.cs.au.dk/dWebTek/2011");
		String contextPath = request.getContextPath();		
		HttpSession session = request.getSession();
        
		Document createItemRequest = null;
		Document modifyItemRequest = null;
		Document adjustItemStockRequest = null;
		
        String itemID;
        String itemName = request.getParameter("itemName");
        String itemPrice = request.getParameter("itemPrice");
        String itemURL = request.getParameter("itemURL");
        String itemDescription = request.getParameter("itemDescription");
        String itemStock = request.getParameter("initStock");
        
        // Sanity checks
        if(itemName==null || itemPrice==null || itemURL==null || itemDescription==null || itemStock==null)
		{
			response.sendRedirect(response.encodeRedirectURL(contextPath + "?error=" + URLEncoder.encode("Missing Argument", "UTF-8")));
			return;
		}
		
		try
		{   /* Build a new Document object from the createItemRequest.xml file. */
			createItemRequest = new SAXBuilder().build(new File(getServletContext().getRealPath("createItemRequest.xml")));
			createItemRequest.getRootElement().getChild("shopKey", w).setText(shopKey);
			createItemRequest.getRootElement().getChild("itemName", w).setText(itemName);
		}
		catch(Exception e)
		{
			PrintWriter out = response.getWriter();
			out.write("FUCK!");
			out.close();
		}
        
        // send request.createItem
		Document createItemResponse = null;
		try
		{
			createItemResponse = cloud.request("createItem", createItemRequest);
		}
		catch(BadUrlResponse e)
		{
			if (e.getResponseCode() == HttpURLConnection.HTTP_BAD_REQUEST )
			{
				response.sendRedirect(response.encodeRedirectURL(contextPath + "?error=" + URLEncoder.encode("Could not create new item!", "UTF-8")));
				return;
			}
			else
			{
                String[] to = getServletContext().getInitParameter("email-devs").split(" ");
                mail.send(to, "", "Bad response codeerror", "Thrown from: CreateItemServlet.createItem. Value: " + e.getResponseCode());
			}
		}
		
        // Request handled correctly for createItem (response is not empty)
		if (createItemResponse!=null)
		{            
            //extract itemID from the valid response (There is only one element in the response, eg itemID. Just return text)
            itemID = createItemResponse.getRootElement().getText();
            Document modifyItemResponse = null;
            
            // make a new request/document with the data applied.
            try
            {
                /* Build a new Document object from the modifyItemRequest.xml file. */
                modifyItemRequest = new SAXBuilder().build(new File(getServletContext().getRealPath("modifyItemRequest.xml")));
                modifyItemRequest.getRootElement().getChild("shopKey", w).setText(shopKey);
                modifyItemRequest.getRootElement().getChild("itemID", w).setText(itemID);
                modifyItemRequest.getRootElement().getChild("itemName", w).setText(itemName);
                modifyItemRequest.getRootElement().getChild("itemPrice", w).setText(itemPrice);
                modifyItemRequest.getRootElement().getChild("itemURL", w).setText(itemURL);
                modifyItemRequest.getRootElement().getChild("itemDescription", w).setText(itemDescription);
            }
            catch(Exception e)
            {
                PrintWriter out = response.getWriter();
                out.write("FUCK!");
                out.close();
            }
            
            // send modifyItemRequest to update the item with specified data
            try
            {
                cloud.request("modifyItem", modifyItemRequest);
            }
            catch(BadUrlResponse e)
            {
                if (e.getResponseCode() == HttpURLConnection.HTTP_BAD_REQUEST )
                {
                     response.sendRedirect(response.encodeRedirectURL(contextPath + "?error=" + URLEncoder.encode("Could not modify item!", "UTF-8")));
                     return;
                }
                else
                {
                     String recv = getServletContext().getInitParameter("email-devs");
                     String[] to = recv.split(" ");
                     mail.send(to, "", "Bad response codeerror", "Thrown from: ModifyItemServlet. Value: " + e.getResponseCode());
                }
            }
          
            // Make a new request to modify item stock
            try
            {
                /* Build a new Document object from the adjustItemStockRequest.xml file. */
                adjustItemStockRequest = new SAXBuilder().build(new File(getServletContext().getRealPath("adjustItemStockRequest.xml")));
                adjustItemStockRequest.getRootElement().getChild("shopKey", w).setText(shopKey);
                adjustItemStockRequest.getRootElement().getChild("itemID", w).setText(itemID);
                adjustItemStockRequest.getRootElement().getChild("adjustment", w).setText(itemStock);
            }
            catch (Exception e)
            {
                PrintWriter out = response.getWriter();
                out.write("FUCK!");
                out.close();
            }
            
            // Send request.adjustItemStockRequest to the cloud (ignore return doc's)
            try
            {
                cloud.request("adjustItemStock", adjustItemStockRequest);
            }
            catch(BadUrlResponse e)
            {
                if (e.getResponseCode() == HttpURLConnection.HTTP_BAD_REQUEST )
                {
                    response.sendRedirect(response.encodeRedirectURL(contextPath + "?error=" + URLEncoder.encode("Unable to adjust stock!", "UTF-8")));
                }
                else
                {
                    String recv = getServletContext().getInitParameter("email-devs");
                    String[] to = recv.split(" ");
                    mail.send(to, "", "Bad response codeerror", "Thrown from: CreateItemServlet:ModifyItemStock. Value: " + e.getResponseCode());
                    response.sendRedirect(response.encodeRedirectURL(contextPath + "?error=" + URLEncoder.encode("Could not apply initial item stock!", "UTF-8")));
                    return;
                }
            }
        }
        else
        {
            String recv = getServletContext().getInitParameter("email-devs");
            String[] to = recv.split(" ");
            mail.send(to, "", "createItemResponse", "Thrown from: CreateItemServlet:CreateItem. CreateItemResponse is null");
            response.sendRedirect(response.encodeRedirectURL(contextPath + "?error=" + URLEncoder.encode("Could not create item!", "UTF-8")));
            return;
        }
        
        response.sendRedirect(response.encodeRedirectURL(contextPath + "/backstore/backstore.jsp"));
    }
}