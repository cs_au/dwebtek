import java.io.*;
import java.net.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import org.jdom.*;
import org.jdom.input.*;
import org.jdom.output.*;
import org.jdom.transform.*;

import skeen.pair;

public class AddServlet extends HttpServlet
{
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        HttpSession session = request.getSession();
        String contextPath = request.getContextPath();
		
        Document sellItemsRequest = null;

        String itemID = request.getParameter("itemID");
        String itemName = request.getParameter("itemName");
        String saleAmount = request.getParameter("saleAmount");
        String itemStockString = request.getParameter("itemStock");
        int itemStock = Integer.parseInt(itemStockString);

        if(itemID==null || saleAmount==null)
        {
             response.sendRedirect(response.encodeRedirectURL(contextPath + "?error=" + URLEncoder.encode("Missing Argument", "UTF-8")));
             return;
        }
        else
        {
            try
            {
                if(Integer.parseInt(saleAmount) < 1)
                {
                    response.sendRedirect(response.encodeRedirectURL(contextPath + "?error=" + URLEncoder.encode("saleAmount should be positive", "UTF-8")));
                    return;
                }
            }
            catch(NumberFormatException e)
            {
                response.sendRedirect(response.encodeRedirectURL(contextPath + "?error=" + URLEncoder.encode("saleAmount not a parseable number!", "UTF-8")));
                return;
            }
        }
        
        Map cart = (Map)session.getAttribute("cart");

        pair data = (pair) cart.get(itemName);
        if(data!=null)
        {
            int number = ((Integer) data.getSecond())+Integer.parseInt(saleAmount);
            if(itemStock<number)
            {
                response.sendRedirect(response.encodeRedirectURL(contextPath + "?error=" + URLEncoder.encode("You cant buy more items then theres currently in stock", "UTF-8")));
                session.setAttribute("cart", cart);
                return;
            }
            else
            {
                cart.put(itemName, new pair(Integer.parseInt(itemID), number)); 
            }
        }
        else
        {
            if(itemStock<Integer.parseInt(saleAmount))
            {
                response.sendRedirect(response.encodeRedirectURL(contextPath + "?error=" + URLEncoder.encode("You cant buy more items then theres currently in stock", "UTF-8")));
                session.setAttribute("cart", cart);
                return;
            }
            cart.put(itemName, new pair(Integer.parseInt(itemID), Integer.parseInt(saleAmount))); 
        }
        session.setAttribute("cart", cart);
        response.sendRedirect(response.encodeRedirectURL(contextPath));
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        doPost(request, response);
    }
}

