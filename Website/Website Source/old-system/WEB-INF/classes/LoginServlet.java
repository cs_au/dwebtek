import java.io.*;
import java.net.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import org.jdom.*;
import org.jdom.input.*;
import org.jdom.output.*;
import org.jdom.transform.*;

import skeen.*;

public class LoginServlet extends HttpServlet
{
    public void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException
    {
        HttpSession session = request.getSession();
        String contextPath = request.getContextPath();
        Namespace w = Namespace.getNamespace("http://www.cs.au.dk/dWebTek/2011");
        Document loginRequest = null;


        String customerName = request.getParameter("customerName");
        String customerPass = request.getParameter("customerPass");


                // Sanity checks
        if(customerName==null || customerPass==null)
        {
             response.sendRedirect(response.encodeRedirectURL(contextPath + "?error=" + URLEncoder.encode("Missing Argument", "UTF-8")));
             return;
        }
        else if(customerPass.length() < 3)
        {
             response.sendRedirect(response.encodeRedirectURL(contextPath + "?error=" + URLEncoder.encode("Password is too short", "UTF-8")));
             return;
        }
        else if(customerName.length() < 2)
        {
             response.sendRedirect(response.encodeRedirectURL(contextPath + "?error=" + URLEncoder.encode("Username is too short", "UTF-8")));
             return;
        }
                
                try
                {
                        /* Build a new Document object from the loginRequest.xml file.
                           Set the object's customerName and customerPass element values
                           to the ones from the HTTP request. */
                        loginRequest = new SAXBuilder().build(new File(getServletContext().getRealPath("loginRequest.xml")));
                        loginRequest.getRootElement().getChild("customerName", w).setText(customerName);
                        loginRequest.getRootElement().getChild("customerPass", w).setText(customerPass);
                }
                catch (Exception e)
                {
                        PrintWriter out = response.getWriter();
                        out.write("FUCK!");
                        out.close();
                }
                
                // Request a loginResponse from the cloud
        Document loginResponse = null;
        try
        {
            loginResponse = cloud.request("login", loginRequest);
        }
        catch (BadUrlResponse e)
        {   
            if (e.getResponseCode() == HttpURLConnection.HTTP_BAD_REQUEST )
            {
                response.sendRedirect(response.encodeRedirectURL(contextPath + "?error=" + URLEncoder.encode("Invalid username/password!", "UTF-8")));
                return;
            }
            else
            {
                String recv = getServletContext().getInitParameter("email-devs");
                String[] to = recv.split(" ");
                mail.send(to, "", "Bad response codeerror", "Thrown from: LoginServlet. Value: " + e.getResponseCode());
            }
        }


        // User could log in!
        if(loginResponse!=null)
        {            
            String customerID = loginResponse.getRootElement().getChildText("customerID", w);
            session.setAttribute("customerID", customerID);
            session.setAttribute("customerName", customerName);
            response.sendRedirect(response.encodeRedirectURL(contextPath));
            return;
        }
        else
        {
            String recv = getServletContext().getInitParameter("email-devs");
            String[] to = recv.split(" ");
            mail.send(to, "", "loginResponse", "Thrown from: LoginServlet. loginResponse is null");
            response.sendRedirect(response.encodeRedirectURL(contextPath + "?error=" + URLEncoder.encode("Invalid username/password!", "UTF-8")));
            return;
        }
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        doPost(request, response);
    }
}