import java.io.*;
import java.net.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import org.jdom.*;
import org.jdom.input.*;
import org.jdom.output.*;
import org.jdom.transform.*;

import skeen.*;

public class ShopsServlet extends HttpServlet
{
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        Document shops= null;
        try
        {
            shops = cloud.request("listShops");
        }
        catch (BadUrlResponse e)
        {
            if (e.getResponseCode() == HttpURLConnection.HTTP_BAD_REQUEST )
            {
                // user made bad request
                response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + "?error=" + URLEncoder.encode("Could not show list shops!", "UTF-8")));
                return;
            }
            else
            {
                String recv = getServletContext().getInitParameter("email-devs");
                String[] to = recv.split(" ");
                mail.send(to, "", "Bad response codeerror", "Thrown from: ShopsServlet. Value: " + e.getResponseCode());
            }
        }
        
        try 
        {
            XSLTransformer t = new XSLTransformer(getServletContext().getRealPath("shops.xsl"));
            Document h = t.transform(shops);
            XMLOutputter outputter = new XMLOutputter();
            String xhtml = outputter.outputString(h);
            request.getSession().setAttribute("shopsTable", xhtml);
        }
        catch(Exception e)
        {
        }
    }
    
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        doGet(request, response);
    }
}
