import java.io.*;
import java.net.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import org.jdom.*;
import org.jdom.input.*;
import org.jdom.output.*;
import org.jdom.transform.*;

import skeen.*;

public class ItemsServlet extends HttpServlet
{
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {  
        HttpSession session = request.getSession();

        String shopID = getServletContext().getInitParameter("shopID");

        Document items = null;
        try
        {
            items = cloud.request("listItems?shopID=" + shopID);
        }
        catch (BadUrlResponse e)
        {
            if (e.getResponseCode() == HttpURLConnection.HTTP_BAD_REQUEST )
            {
                // user made bad request
                response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + "?error=" + URLEncoder.encode("Could not show list items!", "UTF-8")));
                return;
            }
            else
            {
                String recv = getServletContext().getInitParameter("email-devs");
                String[] to = recv.split(" ");
                mail.send(to, "daimler@gmail.com", "Bad response codeerror", "Thrown from: ItemsServlet. Value: " + e.getResponseCode());
            }
        }

        /* 
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        */
        try 
        {
            XSLTransformer t = new XSLTransformer(getServletContext().getRealPath("items.xsl"));
            Document h = t.transform(items);
            XMLOutputter outputter = new XMLOutputter();
            String xhtml = outputter.outputString(h);

            session.setAttribute("itemsTable", xhtml);

            t = new XSLTransformer(getServletContext().getRealPath("itemsLoggedIn.xsl"));
            h = t.transform(items);
            xhtml = outputter.outputString(h);
            session.setAttribute("itemsTableLoggedIn", xhtml);            
        }
        catch(Exception e)
        {
        //    out.println("Exception trying to xsl transform!");
        }
        //out.close();
    }
    
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        doGet(request, response);
    }
}
