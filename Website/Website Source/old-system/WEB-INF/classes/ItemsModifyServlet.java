import java.io.*;
import java.net.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import org.jdom.*;
import org.jdom.input.*;
import org.jdom.output.*;
import org.jdom.transform.*;

import skeen.*;

public class ItemsModifyServlet extends HttpServlet
{
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String shopID = getServletContext().getInitParameter("shopID");
        
        Document items = null;
        try
        {
            items = cloud.request("listItems?shopID=" + shopID);
        }
        catch (BadUrlResponse e)
        {
            if (e.getResponseCode() == HttpURLConnection.HTTP_BAD_REQUEST )
            {
                // user made bad request
                response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + "?error=" + URLEncoder.encode("Could not show list items!", "UTF-8")));
                return;
            }
            else 
            {
                String recv = getServletContext().getInitParameter("email-devs"); 
                String[] to = recv.split(" ");
                mail.send(to, "daimler@gmail.com", "Bad response codeerror", "Thrown from: ItemsModifyServlet. Value: " + e.getResponseCode());
            }
        }
        
        HttpSession session = request.getSession();

        try 
        {
            XSLTransformer t = new XSLTransformer(getServletContext().getRealPath("/backstore/itemsModify.xsl"));
            Document h = t.transform(items);
            String xhtml = new XMLOutputter().outputString(h);
            session.setAttribute("itemsTableModify", xhtml);
        }
        catch(Exception e)
        {
            PrintWriter out = response.getWriter();
			out.write("itemsModify Error");
            out.close();
        }
    }
    
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        doGet(request, response);
    }
}