import javax.servlet.*;
import java.util.*;

import skeen.*;

public class ServerShutDownNotifier implements ServletContextListener
{
	public void contextInitialized(ServletContextEvent contextEvent)
    {
	}
	
    public void contextDestroyed(ServletContextEvent contextEvent)
    {
        String param = contextEvent.getServletContext().getInitParameter("Mail-on-shutdown");
        if(param == null)
            return;
        else if(!param.equals("TRUE"))
            return;

        String recv = contextEvent.getServletContext().getInitParameter("Mail-on-shutdown-targets");
        String[] to = recv.split(" ");
        mail.send(to, "daimler@gmail.com", "Server shutdown", "It appears the server has shutdown!");
    }
}

