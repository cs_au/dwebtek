import java.io.*;
import java.net.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import org.jdom.*;
import org.jdom.input.*;
import org.jdom.output.*;
import org.jdom.transform.*;

import skeen.*;

public class ModifyItemServlet extends HttpServlet
{    
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        doPost(request, response);
    }
    
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String shopKey = getServletContext().getInitParameter("shopKey");
        Namespace w = Namespace.getNamespace("http://www.cs.au.dk/dWebTek/2011");
        String contextPath = request.getContextPath();
        HttpSession session = request.getSession();
        
		Document modifyItemRequest = null;
		Document adjustItemStockRequest = null;
        
        String itemName = request.getParameter("itemName");
        String itemPrice = request.getParameter("itemPrice");
        String itemURL = request.getParameter("itemURL");
        String itemDescription = request.getParameter("itemDescription");
        String itemID = request.getParameter("itemID");
        String itemStockAdjusted = request.getParameter("itemStockAdjusted");
        String itemStockOriginal = request.getParameter("itemStockOriginal");     
        
        //sanity checks
        if(itemName==null || itemID==null || itemPrice==null || itemURL==null || itemDescription==null || itemStockAdjusted==null || itemStockOriginal==null)
        {
             response.sendRedirect(response.encodeRedirectURL(contextPath + "?error=" + URLEncoder.encode("Missing Argument", "UTF-8")));
             return;
        }
        
        try
        {
            modifyItemRequest = new SAXBuilder().build(new File(getServletContext().getRealPath("modifyItemRequest.xml")));
			modifyItemRequest.getRootElement().getChild("shopKey", w).setText(shopKey);
			modifyItemRequest.getRootElement().getChild("itemID", w).setText(itemID);
			modifyItemRequest.getRootElement().getChild("itemName", w).setText(itemName);
			modifyItemRequest.getRootElement().getChild("itemPrice", w).setText(itemPrice);
			modifyItemRequest.getRootElement().getChild("itemURL", w).setText(itemURL);
			modifyItemRequest.getRootElement().getChild("itemDescription", w).setText(itemDescription);
        }
        catch(Exception e)
        {
			PrintWriter out = response.getWriter();
			out.write("FUCK!");
			out.close();
        }
        
        try
        {   //ignored response (because it is not defined according to the project specifications)
            cloud.request("modifyItem", modifyItemRequest);
        }
        catch(BadUrlResponse e)
        {
            if (e.getResponseCode() == HttpURLConnection.HTTP_BAD_REQUEST )
            {
                response.sendRedirect(response.encodeRedirectURL(contextPath + "?error=" + URLEncoder.encode("Could not modify item!", "UTF-8")));
                return;
            }
            else
            {
                String recv = getServletContext().getInitParameter("email-devs");
                String[] to = recv.split(" ");
                mail.send(to, "", "Bad response codeerror", "Thrown from: ModifyItemServlet. Value: " + e.getResponseCode());
            }
        }
        
        // AdjustItemStock
        
        //calculated adjustment of itemStock
        String adjustment = "" + (Integer.parseInt(itemStockAdjusted) - Integer.parseInt(itemStockOriginal));
        
        try
		{   // Build a new Document object from the adjustItemStockRequest.xml file. 
			adjustItemStockRequest = new SAXBuilder().build(new File(getServletContext().getRealPath("adjustItemStockRequest.xml")));
			adjustItemStockRequest.getRootElement().getChild("shopKey", w).setText(shopKey);
			adjustItemStockRequest.getRootElement().getChild("itemID", w).setText(itemID);
			adjustItemStockRequest.getRootElement().getChild("adjustment", w).setText(adjustment);
		}
		catch (Exception e)
		{
			PrintWriter out = response.getWriter();
			out.write("FUCK!");
			out.close();
		}
		
		// send the adjustItemStockRequest to the cloud
        try
        {
            cloud.request("adjustItemStock", adjustItemStockRequest);
        }
        catch(BadUrlResponse e)
        {
            if (e.getResponseCode() == HttpURLConnection.HTTP_BAD_REQUEST )
            {
                response.sendRedirect(response.encodeRedirectURL(contextPath + "?error=" + URLEncoder.encode("Unable to adjust stock!", "UTF-8")));
            }
            else
            {
                String recv = getServletContext().getInitParameter("email-devs");
                String[] to = recv.split(" ");
                mail.send(to, "", "Bad response codeerror", "Thrown from: ModifyItemServlet:adjustItemStock. Value: " + e.getResponseCode());
            }
        }
        response.sendRedirect(response.encodeRedirectURL(contextPath + "/backstore/backstore.jsp"));
    }
    
}

