import java.io.*;
import java.net.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import org.jdom.*;
import org.jdom.input.*;
import org.jdom.output.*;

public class Controller extends HttpServlet
{	
	public void doGet(HttpServletRequest request, HttpServletResponse response)
		throws IOException, ServletException
	{
		HttpSession session = request.getSession();
		String command = request.getServletPath();
		
		if (command.equals("/"))
		{
			getServletContext().getRequestDispatcher("/frontpage.jsp").forward(request,response);
		}
		else if (command.equals("/register"))
		{
			getServletContext().getRequestDispatcher("/register.jsp").forward(request,response);
		}  
		else if (command.equals("/redirect"))
		{
			getServletContext().getRequestDispatcher("/redirect.jsp").forward(request,response);
		}
        else if (command.equals("/logout"))
		{
			getServletContext().getRequestDispatcher("/logout.jsp").forward(request,response);
		}
        else if (command.equals("/itemSold"))
		{
			getServletContext().getRequestDispatcher("/itemSold.jsp").forward(request,response);
		}
        else if (command.equals("/adminLogin"))
        {
			getServletContext().getRequestDispatcher("/adminLogin.jsp").forward(request,response);
        }
        else if (command.equals("/modifyItem"))
        {
			getServletContext().getRequestDispatcher("/modifyItem.jsp").forward(request,response);
        }
        else if (command.equals("/backstoresite"))
        {
            getServletContext().getRequestDispatcher("/backstore/backstore.jsp").forward(request,response);
        }
        else if (command.equals("/adminlogout"))
        {
            getServletContext().getRequestDispatcher("/adminlogout.jsp").forward(request,response);
        }
	}
}
