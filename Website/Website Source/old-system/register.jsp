<!-- This file is the register document. It includes header.jsp -->
<!-- This provides a form for users to register -->
<html>
    <head>
        <title>HorsePower Webshop - New User</title>
    </head>
    <body>
        <%@ include file="header.jsp" %>
        <%@ include file="shops.jsp" %>
            <%
            String error = request.getParameter("error");
            if(error!=null)
            {
                out.print("<b><font color=red>" + error + "</font></b>");
            }

            %>
        <!-- Register form-->
        Form:
        <form method="post" action="registerServlet">
            <label>Name</label>
            <input type="text" name="customerName"/><br/> <!-- fin info om krav -->
            <label>Password</label>
            <input type="password" name="customerPass"/><br/>
            <label>Repeat Password</label>
            <input type="password" name="customerPassRepeat"/><br/>
            <!-- Commit -->
            <input type="submit" name="submit" value="Register" /> <!-- on pressed show relevant information... -->
        </form>
    </body>
</html>
