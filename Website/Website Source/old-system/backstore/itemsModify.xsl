<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:w="http://www.cs.au.dk/dWebTek/2011" version="1.0">
	<xsl:template match="/">
		<table border="1">
				<!-- Create new item form -->
			<form method="post" action="../createItemServlet">
				<tr>
					<td>
						<label>URL: </label><input type="text" name="itemURL"/>
					</td>
					<td>
						<label>Name: </label><input type="text" name="itemName"/>
					</td>
					<td>
						<label>Price: </label><input type="text" name="itemPrice"/>
					</td>
				</tr>
				<tr>
					<td rowspan="2"/>
					<td rowspan="2">
						<label>Description: </label><input type="text" name="itemDescription"/>
					</td>
					<!--<td> <label>ID: </label><input type="text" name="itemID"/> </td> -->
					<td>
						<label>Initial Stock: </label><input type="text" name="initStock" value="0"/>
					</td>
				</tr>
				<tr>
					<td>
						<input type="submit" name="submit" value="Create New Item"/>
					</td>
				</tr>
			</form>
				<!-- Modify existing items form -->
			<xsl:for-each select="w:items/w:item">
				<form method="post" action="../modifyItemServlet">
					<xsl:variable name="itemName">
						<xsl:value-of select="w:itemName"/>
					</xsl:variable>
					<xsl:variable name="itemURL">
						<xsl:value-of select="w:itemURL"/>
					</xsl:variable>
					<xsl:variable name="itemPrice">
						<xsl:value-of select="w:itemPrice"/>
					</xsl:variable>
					<xsl:variable name="itemID">
						<xsl:value-of select="w:itemID"/>
					</xsl:variable>
					<xsl:variable name="itemDesc">
						<xsl:value-of select="w:itemDescription"/>
					</xsl:variable>
					<xsl:variable name="stock">
						<xsl:value-of select="w:itemStock"/>
					</xsl:variable>
					<tr>
						<td>
							<label>URL: </label>
							<input type="text" name="itemURL" value="{$itemURL}"/>
						</td>
						<td>
							<label>Name: </label>
							<input type="text" name="itemName" value="{$itemName}"/>
						</td>
						<td>
							<label>Price: </label>
							<input type="text" name="itemPrice" value="{$itemPrice}"/>
						</td>
					</tr>
					<tr>
						<td rowspan="2">
							<img src="{$itemURL}" alt="LOL"/>
						</td>
						<td>
							<label>Description: </label>
							<input type="text" name="itemDescription" value="{$itemDesc}"/>
						</td>
						<td>
							<label>ID: </label>
							<input type="text" name="itemID" value="{$itemID}" readonly="readonly"/>
						</td>
					</tr>
					<tr>
						<td>
							<label>Stock: </label>
							<input type="text" name="itemStockAdjusted" value="{$stock}"/>
							<input type="hidden" name="itemStockOriginal" value="{$stock}"/>
						</td>
						<td>
							<input type="submit" name="submit" value="Apply Changes"/>
						</td>
					</tr>
				</form>
			</xsl:for-each>
		</table>
	</xsl:template>
</xsl:stylesheet>
