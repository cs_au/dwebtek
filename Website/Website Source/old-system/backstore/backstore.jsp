<!-- This is backstore.jsp. It includes createItem, modifyItems and header -->

<html>
    <head>
        <title>HorsePower Webshop - Backstore</title>
    </head>
    <body>
        <%@ include file="../header.jsp" %>
        <%
        String error = request.getParameter("error");
        if(error!=null)
        {
            out.print("<b><font color=red>" + error + "</font></b>");
        }
        %>
        
        <%@ include file="modifyItem.jsp" %>
        Goto <a href="../">frontstore</a>!
    </body>
</html>

