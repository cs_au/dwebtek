<% session.setAttribute("mainPage", "shoplist"); %>

<jsp:include page="/WEB-INF/modules/header.jsp"/>

    <h1>Other shops</h1>
    <jsp:include page="/WEB-INF/modules/shopslist.jsp"/>

<jsp:include page="/WEB-INF/modules/footer.jsp"/>
