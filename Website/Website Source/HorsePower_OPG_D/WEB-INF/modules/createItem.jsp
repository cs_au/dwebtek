<div id="items">
<form method="post" action="createItem">
	<table>
		<tr>
			<td class="img">
				<img src="http://worlddanceexchange.com/wp-content/uploads/2010/04/camera_icon_1.gif"/><br/>
				<label>(Image) URL: </label><input name="itemURL"/>
			</td>
			<td class="desc">
				<input type="hidden" name="mainPage" value="${mainPage}"/>				
				<label>Name: </label><br/>
				<input name="itemName" value="${item.getName()}"/><br/>
				<label>Price: </label><br/>
				<input name="itemPrice" value="${item.getPrice()}"/><br/>
				<label>Initial stock: </label><br/>
				<input name="initStock" value="${item.getStock()}"/><br/>
				<label>Description: </label><br/>
				<textarea name="itemDescription" cols="40" rows="6"></textarea><br/><br/>
                <c:if test="${(param.createitemerror != null)}"><span class="error">${param.createitemerror}</span><br/><br/></c:if>
				<input type="submit" name="modify" value="Create item"/><br/><br/>
			</td>
		</tr>
	</table>
</form>
<br/><br/>
</div>
