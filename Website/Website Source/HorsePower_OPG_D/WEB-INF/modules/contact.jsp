<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div id="contact">
    <c:if test="${param.contactError != null}"><span class="error">${param.contactError}</span><br/><br/></c:if>
    <form method="post" action="contactDevs">
        <input type="hidden" name="mainPage" value="${mainPage}"/>
		<label>Your name: </label><br/>
		<input type="text" name="name"/><br/><br/>
		<label>Your email: </label><br/>
		<input type="text" name="email"/><br/><br/>
		<label>Message: </label><br/>
		<textarea cols="40" rows="7" name="message"></textarea><br>
		<input type="submit" name="submit" value="Send mail"/>
	</form>
</div>
