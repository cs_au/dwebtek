<!-- !Dep-Mod=menu.jsp -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "DTD/xhtml1-transitional.dtd">
<?xml version="1.0" encoding="UTF-8"?> 

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <title>${title}</title>
        <meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
		<link rel="stylesheet" type="text/css" href="http://andreas.netne.net/dWebTek/style.css"/>  

        <script type="text/javascript">
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-21793427-1']);
            _gaq.push(['_trackPageview']);

            (function() 
             {
                var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();
        </script> 
    </head>
	
	<body>
		<div id="wrapper">
			<div id="banner"></div>
			<jsp:include page="/WEB-INF/modules/menu.jsp"/>
			<div id="content">
