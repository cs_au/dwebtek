<!-- !Dep-Mod=shoppingcart.jsp -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div id="adminLogin">
	<h2>Log in</h2>
		<form method="post" action="j_security_check">
		<input type="hidden" name="mainPage" value="${mainPage}"/>
		<label>Username: </label><br/>
		<input type="text" name="j_username"/><br/><br/>
		<label>Password: </label><br/>
		<input type="password" name="j_password"/><br/>
        <c:if test="${param.adminloginerror != null}"><span class="error">${param.adminloginerror}</span><br/><br/></c:if>
		<input type="submit" name="submit" value="Log in!"/>
	</form>
</div>