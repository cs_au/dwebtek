<!-- !Dep-Bean=Item -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div id="items">
<c:forEach items="${items.matches}" var="item">
<hr/>
	<a name="item=${item.getID()}"></a> <!-- anchor for making in page linking after submit -->
	<form method="post" action="modifyItem">
		<table>
			<tr>
				<td class="img">
					<img src="${item.getURL()}"/><br/>
					<label>(Image) URL: </label><input name="itemURL" value="${item.getURL()}"/>
				</td>
				<td class="desc">
					<input type="hidden" name="mainPage" value="${mainPage}"/>
					
					<!-- hidden fields to tjek if only necessary to send 1 of modifyItemRequest and modifyItemStockRequest -->
					<input type="hidden" name="itemID" value="${item.getID()}"/>
					<input type="hidden" name="itemNameOriginal" value="${item.getName()}"/>
					<input type="hidden" name="itemURLOriginal" value="${item.getURL()}"/>
					<input type="hidden" name="itemPriceOriginal" value="${item.getPrice()}"/>
					<input type="hidden" name="itemStockOriginal" value="${item.getStock()}"/>
					<input type="hidden" name="itemDescriptionOrig" value="${item.getDescription()}"/>
					
					<label>Name: </label><br/>
					<input name="itemName" value="${item.getName()}"/><br/>
					<label>Price: </label><br/>
					<input name="itemPrice" value="${item.getPrice()}"/><br/>
					<label>Stock: </label><br/>
					<input name="itemStockAdjusted" value="${item.getStock()}"/><br/>
					<label>Description: </label><br/>
					<textarea name="itemDescription" cols="40" rows="6">${item.getDescription()}</textarea><br/><br/>
                    
					<c:if test="${(param.modifyitemerror != null) && (item.getID() eq param.item)}"><span class="error">${param.modifyitemerror}</span><br/><br/></c:if>
					<input type="submit" name="modify" value="Apply changes"/><br/><br/>
				</td>
				<td class="buy">
					<h3>ID: ${item.getID()}</h3>
				</td>
			</tr>
		</table>
	</form>
</c:forEach>
</div>
