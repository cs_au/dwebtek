<!-- !Dep-Bean=Shop -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

	<ul>
		<c:forEach items="${shops.matches}" var="shop">
			<li>
				<a href="${shop.getURL()}">${shop.getName()}</a>
			</li>
		</c:forEach>
	</ul>
