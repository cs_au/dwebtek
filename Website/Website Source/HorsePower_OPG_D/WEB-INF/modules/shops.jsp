<!-- !Dep-Bean=Shop -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<form method="post" action="redirect">
	<div id="shopdown">
		<select name="selected">
			<c:forEach items="${shops.matches}" var="shop">
				<option value="${shop.getURL()}">${shop.getName()}</option>
			</c:forEach>
		</select>
		<input type="submit" name="submit" value="Go!"/>
	</div>
</form>
