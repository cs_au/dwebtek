<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ taglib uri="/WEB-INF/tlds/SkeenTags.tld" prefix="skeen" %>

<h2>CHARTS</h2>
<h3>Sales by itemID</h3>
<skeen:chart type="p3" width="500" height="200" profile="sales-by-itemID"/>

<h3>Cash by itemID</h3>
<skeen:chart type="p3" width="500" height="200" profile="cash-by-itemID"/>

<h3>Sales by itemName</h3>
<skeen:chart type="p3" width="500" height="200" profile="sales-by-itemName"/>

<h3>Cash by itemName</h3>
<skeen:chart type="p3" width="500" height="200" profile="cash-by-itemName"/>

<h3>Sales by customerID</h3>
<skeen:chart type="p3" width="500" height="200" profile="sales-by-customerID"/>

<h3>Cash by customerID</h3>
<skeen:chart type="p3" width="500" height="200" profile="cash-by-customerID"/>

<h3>Sales by customerName</h3>
<skeen:chart type="p3" width="500" height="200" profile="sales-by-customerName"/>

<h3>Cash by customerName</h3>
<skeen:chart type="p3" width="500" height="200" profile="cash-by-customerName"/>

