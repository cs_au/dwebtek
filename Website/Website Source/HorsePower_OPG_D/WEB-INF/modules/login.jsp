<!-- !Dep-Mod=shoppingcart.jsp -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div id="login">
	<c:if test="${customerID == null}">
		<h2>Log in</h2>
		<form method="post" action="login">
			<input type="hidden" name="mainPage" value="${mainPage}"/>
			<label>Username: </label><br/>
			<input type="text" name="customerName"/><br/><br/>
			<label>Password: </label><br/>
			<input type="password" name="customerPass"/><br/><br/>
			<c:if test="${param.loginerror != null}"><span class="error">${param.loginerror}</span><br/><br/></c:if>
			<input type="submit" name="submit" value="Log in"/> or <a href="newcustomer">register</a>
		</form>
	</c:if>
	<c:if test="${customerID != null}">
		Sup, ${customerName}?<br/>
		<a href="logout">Log out</a><br/><br/>
		<jsp:include page="/WEB-INF/modules/shoppingcart.jsp"/>
	</c:if>
	<br/>
</div>
