import java.io.*;
import java.net.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import org.jdom.*;
import org.jdom.input.*;
import org.jdom.output.*;
import org.jdom.transform.*;

import skeen.*;

public class CreateItemServlet extends ItemServletHelper
{    
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        super.doPost(request, response);
       // shopKey = getServletContext().getInitParameter("shopKey");
		
		// itemID cannot be made manually.
        itemID = null;
        itemName = request.getParameter("itemName");
        itemPrice = request.getParameter("itemPrice");
        itemURL = request.getParameter("itemURL");
        itemDescription = request.getParameter("itemDescription");
        itemStockAdjustment = request.getParameter("initStock");
		
        // Sanity checks, itemURL may be blank.
		if(!itemURL.equals(""))
		{
			try
			{
				URL url = new URL(itemURL);
				URLConnection connection = url.openConnection();
				connection.connect();
			} 
			catch (Exception e) 
			{
                error("itemURL was not found, or not valid.");
				return;
			}
		}
        if(isNullOrBlank(itemName) || isNullOrBlank(itemPrice) || isNullOrBlank(itemDescription) || isNullOrBlank(itemStockAdjustment))
		{
            error("Missing Argument");
			return;
		}
        
        // send request.createItem
		Document createItemResponse = null;
		try
		{
			createItemResponse = cloud.request("createItem", createItemRequest());
		}
		catch(BadUrlResponse e)
		{
			if (e.getResponseCode() == HttpURLConnection.HTTP_BAD_REQUEST )
			{
                error("Could not create new item!");
				return;
			}
			else
			{
                mail.sendDevs("Bad response codeerror" + ". Thrown from: CreateItemServlet.createItem. Value: " + e.getResponseCode());
                error("Could not create new item.");
                return;
			}
		}
        catch(ValidationException e)
        {
            String statementWhatWentWrong = e.getValidationResponseCode();
            error("Data failed validation! ERROR: " + statementWhatWentWrong);
            return;
        }
		
        // Only continue if Request was handled correctly for createItem (response is not empty)
		if (createItemResponse == null)
        {
            // Request was not good.
            mail.sendDevs("createItemResponse. " +"Thrown from: CreateItemServlet:CreateItem. CreateItemResponse is null");	
            error("Could not create item!");
            return;  
        }
        else // request was good
		{  
            //extract itemID from the valid response
            // There is only one element in the response, eg itemID. Just return text
            itemID = createItemResponse.getRootElement().getText();
            
            // send modifyItemRequest to update the item with specified data
            try
            {
                cloud.request("modifyItem", modifyItemRequest());
            }
            catch(BadUrlResponse e)
            {
                if (e.getResponseCode() == HttpURLConnection.HTTP_BAD_REQUEST )
                {
                    error("Could not modify item!");
                    return;
                }
                else
                {
                    mail.sendDevs("Bad response codeerror. " + "Thrown from: ModifyItemServlet. Value: " + e.getResponseCode());
					error("Could not apply item data to new item.");
                    return;
                }
            }
            catch(ValidationException e)
            {
                String statementWhatWentWrong = e.getValidationResponseCode();
                error("Data failed validation! ERROR: " + statementWhatWentWrong);
                return;
            }
            
            // Send request.modifyItemStockRequest to the cloud (ignore return doc's)
            try
            {
                cloud.request("adjustItemStock", modifyItemStockRequest());
            }
            catch(BadUrlResponse e)
            {
                if (e.getResponseCode() == HttpURLConnection.HTTP_BAD_REQUEST )
                {
                    error("Unable to adjust stock!");
                }
                else
                {
                    mail.sendDevs("Bad response codeerror. " + "Thrown from: CreateItemServlet:ModifyItemStock. Value: " + e.getResponseCode());
                    error("Could not apply initial item stock!");
                    return;
                }
                
            }
            catch(ValidationException e)
            {
                String statementWhatWentWrong = e.getValidationResponseCode();
                error("Data failed validation! ERROR: " + statementWhatWentWrong);
                return;
            }
        }
        succesRedirect();
    }
    
    protected void error(String message)
    {
        redirectBackWithMessage(
            "createitemerror",
            message,
            ""
        );
    }
}
