import java.io.File;
import java.io.IOException;
import java.net.URLEncoder;
import java.net.HttpURLConnection;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.jdom.Document;
import org.jdom.Namespace;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;
import skeen.cloud;
import skeen.mail;
import skeen.BadUrlResponse;
import skeen.ValidationException;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import skeen.*;

import org.jdom.*;
import org.jdom.input.*;
import org.jdom.output.*;

public class LoginServlet extends HttpServlet
{
    public void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException
    {
        HttpSession session = request.getSession();
        String previousPage = request.getParameter("mainPage");
		Namespace w = Namespace.getNamespace("http://www.cs.au.dk/dWebTek/2011");

        String customerName = request.getParameter("customerName");
        String customerPass = request.getParameter("customerPass");
		
		// Sanity checks
        if(customerName==null || customerPass==null)
        {
             response.sendRedirect(response.encodeRedirectURL(previousPage + "?loginerror=" + URLEncoder.encode("Missing Argument", "UTF-8")));
             return;
        }
        else if(customerPass.length() < 3)
        {
             response.sendRedirect(response.encodeRedirectURL(previousPage + "?loginerror=" + URLEncoder.encode("Password is too short", "UTF-8")));
             return;
        }
        else if(customerName.length() < 2)
        {
             response.sendRedirect(response.encodeRedirectURL(previousPage + "?loginerror=" + URLEncoder.encode("Username is too short", "UTF-8")));
             return;
        }
		
        String initParameter = getServletContext().getInitParameter("loginRequest-xml");
        if(initParameter == null) mail.sendDevs("Web.xml - initParameter not well defined! - at LoginServlet");
		Document loginRequest = null;
		try
		{
			/* Build a new Document object from the loginRequest.xml file.
			   Set the object's customerName and customerPass element values
			   to the ones from the HTTP request. */
			loginRequest = new SAXBuilder().build(new File(getServletContext().getRealPath(initParameter)));
			loginRequest.getRootElement().getChild("customerName", w).setText(customerName);
			loginRequest.getRootElement().getChild("customerPass", w).setText(customerPass);
		}
		catch (Exception e)
		{
			mail.sendDevs("FileNotFound - " + initParameter + " - requested at LoginServlet!");
		}
		

		// Request a loginResponse from the cloud
        Document loginResponse = null;
        try
        {
            loginResponse = cloud.request("login", loginRequest);
        }
        catch(BadUrlResponse e)
        {
            if (e.getResponseCode() == HttpURLConnection.HTTP_BAD_REQUEST )
            {
                response.sendRedirect(response.encodeRedirectURL(previousPage + "?loginerror=" + URLEncoder.encode(
                    "Could not log in. Please make sure you entered correct username and password or try again later!", "UTF-8")));
                return;
            }
            else
            {
                mail.sendDevs("Bad response codeerror. " + "Thrown from: LoginServlet. Value: " + e.getResponseCode());
            }
        }
        catch(ValidationException e)
        {
            Format format = Format.getPrettyFormat();
            format.setEncoding("UTF-8");
            XMLOutputter out = new XMLOutputter(format);
            
            String statementWhatWentWrong = e.getValidationResponseCode();
            
            response.sendRedirect(response.encodeRedirectURL(previousPage + "?loginerror=" + URLEncoder.encode(
                    "DatavalidationError: "+statementWhatWentWrong +
                    "_____" + out.outputString(loginRequest) , "UTF-8")));
            return;
        }

        // User could log in!
        if(loginResponse!=null)
        {            
            String customerID = loginResponse.getRootElement().getChildText("customerID", w);
            session.setAttribute("customerID", customerID);
            session.setAttribute("customerName", customerName);
        }
        else
        {
            mail.sendDevs("Bad response. " + "Thrown from: LoginServlet.");
            response.sendRedirect(response.encodeRedirectURL(previousPage + "?loginerror=" + URLEncoder.encode(
                "Could not log in. Please make sure you entered correct username and password!", "UTF-8")));
            return;
        }
        
        response.sendRedirect(response.encodeRedirectURL(previousPage));
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException
    {
        doPost(request, response);
    }
}

