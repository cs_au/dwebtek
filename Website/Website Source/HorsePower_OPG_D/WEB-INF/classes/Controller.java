import java.io.File;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import skeen.*;

public class Controller extends HttpServlet
{
	private HttpSession session;

    // Build JDOM from modules.xml
    private static Document modulesDoc = null;
    private static Document pagesDoc = null;
	
	/**
	 * POST requests
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
		throws IOException, ServletException
	{
		this.session = request.getSession();
        chartTag.seedSession(session);
        Item.seedSession(session);
		
		String path = request.getServletPath();
		
		// Get the page element corresponding to the servlet path
		Element page = getPage(path);
		
		/* If the page is not specified in pages.xml, redirect to error page.
		   Otherwise, load beans and redirect to the right page. */
		if (page != null)
		{
			//loadModules(page);
			loadBeans();
			getServletContext().getRequestDispatcher("/WEB-INF" + page.getChildText("link")).forward(request,response);
		}
		else
		{
			getServletContext().getRequestDispatcher("/WEB-INF/utility/pageofshit.jsp").forward(request,response);
		}
	}
	
	/**
	 * GET requests
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
		throws IOException, ServletException
	{
		doPost(request,response);
	}
	
	/**
	 * Preloads pages.xml for quicker look-ups.
	 */
    public static void preload(ServletContext con)
    {
        String initParameter;

        initParameter = con.getInitParameter("pages-xml");
        if(initParameter == null)
            mail.sendDevs("Web.xml - initParameter not well defined! - at Controller");
		try
		{
			pagesDoc = new SAXBuilder().build(new File(con.getRealPath(initParameter)));
		}
		catch (Exception e) 
        {
            mail.sendDevs("Parsing Error At: - " + initParameter + " - requested at Controller!");
        }
        /*
        initParameter = con.getInitParameter("modules-xml");
        if(initParameter == null)
            mail.sendDevs("Web.xml - initParameter not well defined! - at Controller");
        try
        {
            modulesDoc = new SAXBuilder().build(new File(con.getRealPath(initParameter)));
        }
        catch (Exception e)
        {
            mail.sendDevs("Parsing Error At: - " + initParameter + " - requested at Controller!");
        }
		*/
    }
	
	/**
	 * Returns the page-element with the path that matches the
	 * parameter string.
	 */
	private Element getPage(String inputpath)
	{
		// Get all the page-elements in the xml-file
        @SuppressWarnings("unchecked")
        List<Element> pages = pagesDoc.getRootElement().getChildren("page");
		
		/* Find the page-element with the specified path by
		   looping through all its path-elements */
		for (Element page : pages)
		{
			for (Object path : page.getChildren("path"))
			{
				String value = ( (Element)path ).getText();
				if (value.equals(inputpath))
					return page;
			}
		}
		return null;
	}

	/**
	 * Adds session attributes for all beans. This method is
	 * faster than loadModules, but makes necessary resetting
	 * attributes in other servlets.
	 */
	private void loadBeans()
	{
		if (session.getAttribute("items") == null)
		{
            //Sort by cloudReturn (StandardItemSort())
			//Model items = new Model<Item>(new Item());
            //Sort in Inverse, when logged in
			//Model items = new Model<Item>(new Item(null, new InverseItemSort(new StandardItemSort())));
            //Sort by Stock
			//Model items = new Model<Item>(new Item(null, new StockItemSort()));
            //Sort in Inverse by Stock
			//Model items = new Model<Item>(new Item(null, new InverseItemSort(new StockItemSort())));
            //Sort in GoogleSorted mode
			Model items = new Model<Item>(new Item(null, new GoogleItemSort()));
			session.setAttribute("items", items);
		}
		if (session.getAttribute("shops") == null)
		{
			Model shops = new Model<Shop>(new Shop());
			session.setAttribute("shops", shops);
		}
		if (session.getAttribute("cart") == null)
		{
			Model cart = new Model<Cart>(new Cart(session));
			session.setAttribute("cartmodel", cart);
		}
		if (session.getAttribute("sales") == null)
		{
			Model sales = new Model<Sale>(new Sale());
			session.setAttribute("sales", sales);
		}
		if (session.getAttribute("customers") == null)
		{
			Model customers = new Model<Customer>(new Customer());
			session.setAttribute("customers", customers);
		}
	}
	
	/**
	 * Loads (sets session variables for) all modules the
	 * page-element requires, according to modules.xml
	 */
	/*
	private void loadModules(Element page)
	{
        String pagetitle = page.getChildText("title");
        session.setAttribute("title", pagetitle);

        @SuppressWarnings("unchecked")
        List<Element> modules = modulesDoc.getRootElement().getChildren("module");

        for (Object o : page.getChildren("module"))
        {
            Element e = (Element) o;
            String modulename = e.getText();

            for (Element module : modules)
            {
                if (module.getChildText("title").equals(modulename))
                {	
                    if (module.getChildText("bean") == null)
                    {
                    }
                    else if (module.getChildText("bean").equals("Item"))// && session.getAttribute("items")==null)
                    {
                        Model items = new Model<Item>();
                        session.setAttribute("items", items);
                    }
                    else if (module.getChildText("bean").equals("Shop"))// && session.getAttribute("shops")==null)
                    {
                        Model shops = new Model<Shop>();
                        session.setAttribute("shops", shops);
                    }
                    else if (module.getChildText("bean").equals("Cart"))
                    {
                        Model cart = new Model<Cart>();
                        session.setAttribute("cartmodel", cart);
                    }
                    else if (module.getChildText("bean").equals("Sale"))
                    {
                        Model sales = new Model<Sale>();
                        session.setAttribute("sales", sales);
                    }
                    else if (module.getChildText("bean").equals("Customer"))
                    {
                        Model customers = new Model<Customer>();
                        session.setAttribute("customers", customers);
                    }

                    if (module.getChildText("module-dep") != null)
					{
						String moduleDepName = module.getChildText("module-dep");
						for (Element moduleDep : modules)
						{
							if (moduleDep.getChildText("bean") == null)
							{
							}
							else if (moduleDep.getChildText("bean").equals("Item"))// && session.getAttribute("items")==null)
							{
								Model items = new Model<Item>();
								session.setAttribute("items", items);
							}
							else if (moduleDep.getChildText("bean").equals("Shop"))// && session.getAttribute("shops")==null)
							{
								Model shops = new Model<Shop>();
								session.setAttribute("shops", shops);
							}
							else if (moduleDep.getChildText("bean").equals("Cart"))
							{
								Model cart = new Model<Cart>();
								session.setAttribute("cartmodel", cart);
							}
							else if (moduleDep.getChildText("bean").equals("Sale"))
							{
								Model sales = new Model<Sale>();
								session.setAttribute("sales", sales);
							}
							else if (moduleDep.getChildText("bean").equals("Customer"))
							{
								Model customers = new Model<Customer>();
								session.setAttribute("customers", customers);
							}
						}
					}
                }
            }
        }
    }	*/
}
