import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import skeen.mail;

public class ServerShutDownNotifier implements ServletContextListener
{
	public void contextInitialized(ServletContextEvent contextEvent)
    {
	}
	
    public void contextDestroyed(ServletContextEvent contextEvent)
    {
        ServletContext con = contextEvent.getServletContext();
        String param = con.getInitParameter("Mail-on-shutdown");
        if(param == null || !(param.equals("TRUE") || param.equals("FALSE")))
        { 
            // Mail-on-shutdown is not well defined, let the devs know.
            // Either not set, or not true or false
			mail.sendDevs("Web.xml - Mail-on-shutdown not well defined!");
            return;
        }
        else if(!param.equals("TRUE"))
            return;
        
        // Mail on shutdown
        String recv = con.getInitParameter("Mail-on-shutdown-targets");
        String[] to = recv.split(" ");
        mail.send(to, "daimler@gmail.com", "Server shutdown", "It appears the server has shutdown!");
    }
}

