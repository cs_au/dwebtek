import java.io.File;
import java.io.IOException;
import java.net.URLEncoder;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.jdom.Document;
import org.jdom.Namespace;
import org.jdom.input.SAXBuilder;
import skeen.cloud;
import skeen.mail;

public class RegisterServlet extends HttpServlet
{
    public void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException
    {		
        String shopKey = getServletContext().getInitParameter("shopKey");

        HttpSession session = request.getSession();
        String previousPage = request.getParameter("mainPage");
		Namespace w = Namespace.getNamespace("http://www.cs.au.dk/dWebTek/2011");
		Document createCustomerRequest = null;

        String customerName = request.getParameter("customerName");
        String customerPass = request.getParameter("customerPass");
        String customerPassRepeat = request.getParameter("customerPassRepeat");
        
        if(customerName==null || customerPass==null || customerPassRepeat==null)
        {
            response.sendRedirect(response.encodeRedirectURL(previousPage + "?registererror=" + URLEncoder.encode("Missing Argument", "UTF-8")));
            return;
        }
        else if(!customerPassRepeat.equals(customerPass))
        {
            response.sendRedirect(response.encodeRedirectURL(previousPage + "?registererror=" + URLEncoder.encode("The passwords are not identical!", "UTF-8")));
            return;
        }
        else if(customerPass.length() < 3)
        {
            response.sendRedirect(response.encodeRedirectURL(previousPage + "?registererror=" + URLEncoder.encode("Password is too short!", "UTF-8")));
            return;
        }
        else if(customerName.length() < 2)
        {
            response.sendRedirect(response.encodeRedirectURL(previousPage + "?registererror=" + URLEncoder.encode("Username is too short!", "UTF-8")));
            return;
        }
        
        String initParameter = getServletContext().getInitParameter("createCustomerRequest-xml");
        if(initParameter == null)
            mail.sendDevs("Web.xml - initParameter not well defined! - at RegisterServlet"); 
        

        try
        {
            /* Build a new Document object from the loginRequest.xml file.
			   Set the object's customerName and customerPass element values
			   to the ones from the HTTP request. */
			createCustomerRequest = new SAXBuilder().build(new File(getServletContext().getRealPath(initParameter)));
			createCustomerRequest.getRootElement().getChild("shopKey", w).setText(shopKey);
			createCustomerRequest.getRootElement().getChild("customerName", w).setText(customerName);
			createCustomerRequest.getRootElement().getChild("customerPass", w).setText(customerPass);
        }
        catch(Exception e)
        {
            mail.sendDevs("FileNotFound - " + initParameter + " - requested at RegisterServlet!");
        }

        Document createCustomerResponse = null;
        try
        {
            createCustomerResponse = cloud.request("createCustomer", createCustomerRequest);
        }
        catch (Exception e)
        {
		}

        // User was created!
        if(createCustomerResponse!=null)
        {
            String customerID = createCustomerResponse.getRootElement().getChildText("customerID", w);
            session.setAttribute("customerID", customerID);
            session.setAttribute("customerName", customerName);
            
            String usernameTaken = createCustomerResponse.getRootElement().getChildText("usernameTaken", w);
            if(usernameTaken == null)
            {
                response.sendRedirect(response.encodeRedirectURL(previousPage + "?newaccount=true"));
                return;
            }
            else
            {
                response.sendRedirect(response.encodeRedirectURL(previousPage + "?registererror=" + URLEncoder.encode("Username was taken!", "UTF-8")));
                return;
            }
        }
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException
    {
        doPost(request, response);
    }
}


