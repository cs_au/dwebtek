package skeen;

import java.text.StringCharacterIterator;
import java.text.CharacterIterator;

public final class textEscape
{
    private textEscape()
    {
    }

    public static String forHTML(String input)
    {
        String output = "";
        StringCharacterIterator iterator = new StringCharacterIterator(input);

        char character =  iterator.current();
        while(character != CharacterIterator.DONE)
        {
            if (character == '<')
            {
                output  = output.concat("&lt;");
            }
            else if (character == '>') {
                output  = output.concat("&gt;");
            }
            else if (character == '&') {
                output  = output.concat("&amp;");
            }
            else if (character == '\"') {
                output  = output.concat("&quot;");
            }
            else 
            {
                output  = output.concat("" + character);
            }
            character = iterator.next();
        }
        return output;
    }

    public static String forXML(String input)
    {
        String output = "";
        StringCharacterIterator iterator = new StringCharacterIterator(input);

        char character =  iterator.current();
        while(character != CharacterIterator.DONE)
        {
            if (character == '<') {
                output  = output.concat("&lt;");
            }
            else if (character == '>') {
                output  = output.concat("&gt;");
            }
            else if (character == '\"') {
                output  = output.concat("&quot;");
            }
            else if (character == '\'') {
                output  = output.concat("&#039;");
            }
            else if (character == '&') {
                output  = output.concat("&amp;");
            }
            else 
            {
                output  = output.concat("" + character);
            }
            character = iterator.next();
        }
        return output;
    }
}

