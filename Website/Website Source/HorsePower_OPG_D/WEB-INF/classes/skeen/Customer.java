package skeen;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.Namespace;
import org.jdom.output.XMLOutputter;
import skeen.textEscape;

public class Customer implements Listable
{
	private static Namespace w = Namespace.getNamespace("http://www.cs.au.dk/dWebTek/2011");
	private String customerID, customerName;
	
	// Constructor
	public Customer(String customerID, String customerName)
	{
		this.customerID = customerID;
		this.customerName = customerName;
	}
	
	// Empty constructor
	public Customer() 
    {
    }
	
	// Returns the request string
	public String getRequestString()
	{
		return "listCustomers";
	}
	
	// Returns a list of Item objects
	public List<Listable> generateMatches(Document doc)
	{
		List<Listable> result = new ArrayList<Listable>();
		Iterator iterator = doc.getRootElement().getChildren("customer", w).iterator();
		
		while (iterator.hasNext())
		{
			Element e = (Element)iterator.next();
			String id =          e.getChildText("customerID", w);
			String name =        e.getChildText("customerName", w);
			
			Customer customer = new Customer(textEscape.forHTML(id),
											 textEscape.forHTML(name));
			result.add(customer);
		}
		
		return result;
	}
	
	// Get methods used in JSP
	public String getID()          { return customerID; }
	public String getName()        { return customerName; }
}
