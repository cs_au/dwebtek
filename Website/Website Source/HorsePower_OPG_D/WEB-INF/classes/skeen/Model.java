package skeen;

import java.util.List;
import org.jdom.Document;
import skeen.*;

public class Model<T extends Listable>
{
	private Document doc;	
    private T t;
	/*
		When the JavaBean is created, a Document object is made using
		the Listable parameter. The request string for the specific
		type is used to pull XML-data from the cloud.
	*/
	public Model(T init)
    {
        t = init;
        try
        {
		    doc = cloud.request(t.getRequestString());
        }
        catch(BadUrlResponse e)
        {
            mail.sendDevs("BadUrlResponse at Model: "+ e.getResponseCode()); 
        } 
        catch(ValidationException e)
        {
            mail.sendDevs("ValidationException at Model: "+ e.getValidationResponseCode()); 
        }
	}
	
	/*
		getMatches (used in JSP as "foo.matches") generates a list
		of objects implementing the Listable interface.
	*/
	public List<Listable> getMatches()
	{
		return t.generateMatches(doc);
	}
}
