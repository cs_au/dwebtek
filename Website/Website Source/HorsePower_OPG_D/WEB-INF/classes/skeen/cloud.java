package skeen;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import skeen.*;

import org.jdom.*;
import org.jdom.input.*;
import org.jdom.output.*;

public class cloud
{
    private cloud()
    {
    }

    // No data = GET
    public static Document request(String cloud_req_url)
        throws BadUrlResponse, ValidationException
    {
        return request(cloud_req_url, null);
    }
    
    // Data = POST
    public static Document request(String cloud_req_url, Document data)
        throws BadUrlResponse, ValidationException
    {
        Document doc = null;
        try
        {
            HttpURLConnection con = (HttpURLConnection)
                (new URL(("http://services.brics.dk/java/cloud/"
                          + cloud_req_url))).openConnection();
            
            if(data != null)
            {
                validateDoc(data);
                con.setDoOutput(true);
                
				Format format = Format.getPrettyFormat();
				format.setEncoding("UTF-8");
                XMLOutputter out = new XMLOutputter(format);
				out.output(data, con.getOutputStream());
            }
            
            if(con.getResponseCode() == HttpURLConnection.HTTP_OK)
            {
                SAXBuilder b = new SAXBuilder();
                doc = b.build(con.getInputStream());
                
                //validateDoc(doc);
            }
            else
            {
                throw new BadUrlResponse("" + con.getResponseCode());
                //System.out.println("HttoURLConnection Issue; ErrorCode:\t" + con.getResponseCode());
            }
        } 
        catch(IOException e) 
        {
            mail.sendDevs("IOException at cloud"); 
        }
        catch(JDOMException e)
        {
            mail.sendDevs("Saxbuilder failed at cloud"); 
        }
        return doc;
    }
    
    // This could be changed to use pipedStreams instead of using string as mediator
    public static Document validator(Document requestDoc) throws ValidationException
    {
        return (validateDoc(requestDoc)?requestDoc:null);
    }    
    
    private static boolean validateDoc(Document docToBeValidated) throws ValidationException
    {
        return validateXML(
            new org.xml.sax.InputSource(
                new StringReader(
                    (new XMLOutputter()).outputString(
                        docToBeValidated
                    )
                )
            )
        );
    }
    
    private static boolean validateXML(org.xml.sax.InputSource in) throws ValidationException
    {
        boolean validated = true;
        try
        {
            SAXBuilder b = new SAXBuilder();
            
            //validation should occur.
            b.setValidation(true); 
            
            // Allow validation against XML Schema descriptions (referenced in the XML-file).
            b.setProperty(
                "http://java.sun.com/xml/jaxp/properties/schemaLanguage",
                "http://www.w3.org/2001/XMLSchema");
            
            try
            {
                Document d = b.build(in);
            }
            catch(JDOMParseException e)
            {
                validated = false;
                throw new ValidationException(e.getMessage());
            }
        }
        catch(Exception e)
        {
            validated = false;
            throw new ValidationException("" + e);
        }
        return validated;
    }
}
