package skeen;

import skeen.*;
import java.util.*;
import org.jdom.Element;
import org.jdom.Namespace;

public class GoogleItemSort extends SortItems
{
    public GoogleItemSort()
    {
        super();
    }

    public GoogleItemSort(SortItems i)
    {
        super(i);
    }

    public List<Listable> getList(List<Element> tmp)
    {
        List<Listable> result = next.getList(tmp);
        return result;    
    }

    public List<Listable> getList(List<Element> tmp, String custId)
    {
        List<Listable> list = getList(tmp); // vores shop items
        
        Sale s1 = new Sale();
        s1.seedCustomerID(custId);
        List<Listable> sales = new Model<Sale>(s1).getMatches(); // what the customer has bought

        List<List<pair<Item, pair<Sale, GoogleStats>>>> processes = new ArrayList<List<pair<Item, pair<Sale, GoogleStats>>>>();

        for (Listable listable1 : list)
        {   
            processes.add(new ArrayList<pair<Item, pair<Sale, GoogleStats>>>());
            Item i = (Item) listable1;
            for(Listable listable2 : sales)
            {
                Sale s2 = (Sale) listable2;
                String itemName = dataTransformer.itemID_to_ItemName(s2.getShopID(), s2.getItemID());
                if(itemName==null)
                    continue;
                GoogleStats gs = new GoogleStats(i.getName(), itemName);
                gs.start();
                processes.get(processes.size()-1).add(new pair(i, new pair(s2, gs)));
            }
        }
        list.clear();
        for(List<pair<Item, pair<Sale, GoogleStats>>> listy : processes)
        {
            double sumRat = 0;
            for(pair<Item, pair<Sale, GoogleStats>> gs : listy)
            {
                sumRat += gs.get2().get2().getRating();//*Double.parseDouble(gs.get2().get1().getSaleAmount());
            }
            if(!listy.isEmpty())
                listy.get(0).get1().setRating(sumRat);
            for(pair<Item, pair<Sale, GoogleStats>> gs : listy)
            {
                gs.get1().setName("" + gs.get1().getRating());
                list.add(gs.get1());
            }
        }

        return list;
    }
}
