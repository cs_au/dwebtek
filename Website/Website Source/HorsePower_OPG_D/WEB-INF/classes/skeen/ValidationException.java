package skeen;

public class ValidationException extends Exception
{
    private String validationResponseCode = "";
    
    public ValidationException(String code)
    {
        super(code);
        try
        {
            this.validationResponseCode = "" + code;
        }
        catch (Exception e)
        {
			mail.sendDevs("Unparseable data at BadUrlResponse!"); 
        }
    }
    
    public String getValidationResponseCode()
    {
        return "" + validationResponseCode;
    }
}
