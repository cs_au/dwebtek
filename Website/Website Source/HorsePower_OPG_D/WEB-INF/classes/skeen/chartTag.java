package skeen;

import java.io.*;
import java.util.*;
import javax.servlet.jsp.*;
import javax.servlet.jsp.tagext.*;
import javax.servlet.http.HttpSession;

import java.util.List;
import skeen.*;

public class chartTag implements Tag, Serializable 
{
    private PageContext pageContext = null;
    private Tag parent = null;
    private static HttpSession session = null;

    private String type = null;
    private String width = null;
    private String height = null;
    private String profile = null;

    public static void seedSession(HttpSession s)
    {
        session = s;
        dataTransformer.seedSession(s);
    }

    public void setPageContext(PageContext p) 
    {
        pageContext = p;
    }

    public void setParent(Tag t) 
    {
        parent = t;
    }

    public Tag getParent() 
    {
        return parent;
    }

    public void setType(String s) 
    {
        type = s;
    }

    public String getType() 
    {
        return type;
    }

    public void setWidth(String s) 
    {
        width = s;
    }

    public String getWidth() 
    {
        return width;
    }

    public void setHeight(String s) 
    {
        height = s;
    }

    public String getHeight() 
    {
        return height;
    }

    public void setProfile(String s) 
    {
        profile = s;
    }

    public String getProfile() 
    {
        return profile;
    }

    public int doStartTag()
        throws JspException 
    {
        Model sessionData = (Model) session.getAttribute("sales");
        if(sessionData==null)  
        {
            mail.sendDevs("Error getting data at chartTag.java");
            return SKIP_BODY;
        }
        List<Listable> inputData=sessionData.getMatches();
        if(inputData.isEmpty())
        {
            mail.sendDevs("Empty input data at chartTag.java");
            return SKIP_BODY;
        }
        int width_Integer=0;
        int height_Integer=0;
        try
        {
            width_Integer = Integer.parseInt(width);
            height_Integer = Integer.parseInt(height);
        } 
        catch(Exception e)
        {
            mail.sendDevs("Unparsable Integer at chartTag.java");
            return SKIP_BODY;
        }
        List<pair<Integer, String>> data = null;

        //pick based upon profile
        if(profile.equals("sales-by-itemID"))
        {
            data = dataTransformer.sales_by_itemID_transform(inputData);
        }
        else if(profile.equals("cash-by-itemID"))
        {
            data = dataTransformer.cash_by_itemID_transform(inputData);
        }
        else if(profile.equals("sales-by-itemName"))
        {
            data = dataTransformer.sales_by_itemID_transform(inputData);
            data = dataTransformer.itemID_to_ItemName(data);
        }
        else if(profile.equals("cash-by-itemName"))
        {
            data = dataTransformer.cash_by_itemID_transform(inputData);
            data = dataTransformer.itemID_to_ItemName(data);
        }
        else if(profile.equals("sales-by-customerID"))
        {
            data = dataTransformer.sales_by_customerID_transform(inputData);
        }
        else if(profile.equals("cash-by-customerID"))
        {
            data = dataTransformer.cash_by_customerID_transform(inputData);
        }
        else if(profile.equals("sales-by-customerName"))
        {
            data = dataTransformer.sales_by_customerID_transform(inputData);
            data = dataTransformer.customerID_to_customerName(data);
        }
        else if(profile.equals("cash-by-customerName"))
        {
            data = dataTransformer.cash_by_customerID_transform(inputData);
            data = dataTransformer.customerID_to_customerName(data);
        }

        if(data==null || data.isEmpty())
        {
            mail.sendDevs("Bad data at chartTag.java");
            return SKIP_BODY;
        }

        try
        {
            String chart_img = "";
            chart_img = chart_img.concat("<img src=\"");
            chart_img = chart_img.concat(chart.generate(type, width_Integer, height_Integer, data));
            chart_img = chart_img.concat("\" />");
            pageContext.getOut().write(chart_img);          
        }
        catch(IOException e) 
        {
            throw new JspTagException("An IOException occurred.");
        }
        catch(Exception e)
        {
            mail.sendDevs("Bad size at chartTag.java");
        } 
        return SKIP_BODY;
    }

    public int doEndTag()
        throws JspException 
    {
        return EVAL_PAGE;
    }

    public void release() 
    {
        pageContext = null;
        parent = null;
        type = null;
        width = null;
        height = null;
        profile = null;
    }
}

