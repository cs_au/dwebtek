package skeen;

import skeen.*;
import java.util.List;
import org.jdom.Element;
import org.jdom.Namespace;

public abstract class SortItems implements SortItemsInterface
{
    SortItems next = null;
    
    public SortItems()
    {
        next = new listItems();
    }

    public SortItems(SortItems i)
    {
        if(i==null)
        {
            return;
        }
        next = i;
    }

    public abstract List<Listable> getList(List<Element> t);
    public abstract List<Listable> getList(List<Element> t, String a);
}
