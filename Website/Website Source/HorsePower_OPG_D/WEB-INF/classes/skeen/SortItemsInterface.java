package skeen;

import skeen.*;
import java.util.List;
import org.jdom.Element;
import org.jdom.Namespace;

public interface SortItemsInterface
{
    public static final Namespace w = Namespace.getNamespace("http://www.cs.au.dk/dWebTek/2011");
    public List<Listable> getList(List<Element> t);
    public List<Listable> getList(List<Element> t, String a);
}
