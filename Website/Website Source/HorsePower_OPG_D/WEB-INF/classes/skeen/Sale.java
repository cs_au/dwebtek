package skeen;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletContext;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.Namespace;
import org.jdom.output.XMLOutputter;
import skeen.textEscape;

public class Sale implements Listable
{
	private static Namespace w = Namespace.getNamespace("http://www.cs.au.dk/dWebTek/2011");
    private static ServletContext con = null;

    private String saleTime, saleAmount, shopID, itemID, customerID, saleItemPrice;
    private String customerIDSeed;
    public void seedCustomerID(String customerID)
    {
        this.customerIDSeed = customerID;
    }

    public static void seedServletContext(ServletContext init_con)
    {
        con = init_con;
    }

	// Constructor
	public Sale(String saleTime, String saleAmount, String shopID,
				String itemID, String customerID, String saleItemPrice)
	{
		this.saleTime = saleTime;
		this.saleAmount = saleAmount;
		this.shopID = shopID;
		this.itemID = itemID;
		this.customerID = customerID;
		this.saleItemPrice = saleItemPrice;
	}
	
	// Empty constructor
	public Sale() 
    {
    }

	// Returns the request string
	public String getRequestString()
	{
        /*
        String shopKey = con.getInitParameter("shopKey");
        if(shopKey == null)
            mail.sendDevs("Web.xml - shopKey not well defined! - at Sale");
        */  
        if(customerIDSeed == null)
        {
		    return "listShopSales?shopKey=C11BB6903BE16B14A57A1DF6";
        }
        else
        {
            return "/listCustomerSales?customerID=" + customerIDSeed;
        }
	}
	
	// Returns a list of Item objects
	public List<Listable> generateMatches(Document doc)
	{
		List<Listable> result = new ArrayList<Listable>();
		Iterator iterator = doc.getRootElement().getChildren("sale", w).iterator();
		
		while (iterator.hasNext())
		{
			Element e = (Element)iterator.next();
			String saleTime =      e.getChildText("saleTime", w);
			String saleAmount =    e.getChildText("saleAmount", w);
			String shopID =        e.getChildText("shopID", w);
			String itemID =        e.getChildText("itemID", w);
			String customerID =    e.getChildText("customerID", w);
			String saleItemPrice = e.getChildText("saleItemPrice", w);
			
			Sale sale = new Sale(textEscape.forHTML(saleTime),
                                 textEscape.forHTML(saleAmount),
                                 textEscape.forHTML(shopID),
                                 textEscape.forHTML(itemID),
                                 textEscape.forHTML(customerID),
                                 textEscape.forHTML(saleItemPrice));
			result.add(sale);
		}
		
		return result;
	}
	
	// Get methods used in JSP
	public String getSaleTime()      { return saleTime; }
	public String getSaleAmount()    { return saleAmount; }
	public String getShopID()        { return shopID; }
	public String getItemID()        { return itemID; }
	public String getCustomerID()    { return customerID; }
	public String getSaleItemPrice() { return saleItemPrice; }
}
