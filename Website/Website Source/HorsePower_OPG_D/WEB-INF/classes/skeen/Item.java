package skeen;

import java.util.*;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.Namespace;
import org.jdom.output.XMLOutputter;
import skeen.*;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;

public class Item implements Listable, Comparable<Listable>
{
	private static Namespace w = Namespace.getNamespace("http://www.cs.au.dk/dWebTek/2011");

	private String itemID, itemName, itemURL, itemPrice, itemStock, itemDescription;
    private double rating = 0;
    private SortItems loggedOut;
    private SortItems loggedIn;

    private static HttpSession session;

    private String shopIDSeed;
    public void seedShopID(String shopID)
    {
        this.shopIDSeed = shopID;
    }

    
    public static void seedSession(HttpSession initSession)
    {
        session = initSession;
    } 

	// Constructor
	public Item(String itemID, String itemName, String itemURL, String itemPrice, String itemStock, String itemDescription)
	{
		this.itemID = itemID;
		this.itemName = itemName;
		this.itemURL = itemURL;
		this.itemPrice = itemPrice;
		this.itemStock = itemStock;
		this.itemDescription = itemDescription;
	}
	
    //TODO: REMOVE THIS DEBUGGING SHIT
    public void setName(String s)
    {
        itemName = s;
    }
    
    public Item() 
    {
        loggedOut = new StandardItemSort();
        loggedIn = new StandardItemSort();
    } 

    public Item(SortItems out) 
    {
        loggedOut = out;
        loggedIn = new StandardItemSort();
    }
    
    public Item(SortItems out, SortItems in) 
    {
        if(out==null)
        {
            loggedOut = new StandardItemSort();
        }
        else
        {
            loggedOut = out;
        }
        loggedIn = in;
    } 

	// Returns the request string
	public String getRequestString()
	{
        /*
        String shopID = con.getInitParameter("shopID");
        if(shopID == null)
            mail.sendDevs("Web.xml - shopID not well defined! - at Item");
        */
        if(shopIDSeed==null)
        {
		    return "listItems?shopID=74";
        }
        else
        {
            return "listItems?shopID=" + shopIDSeed;
        }
	}
	
    // Returns a list of Item objects
	public List<Listable> generateMatches(Document doc)
	{
        List<Listable> result = new ArrayList<Listable>();
        if (doc == null) return result;
        List<Element> tmp = doc.getRootElement().getChildren("item", w);
        if (tmp == null) return result;
       
        String custId = (String) session.getAttribute("customerID");
        if(custId != null)
        {
            result = loggedIn.getList(tmp, custId);
        }
        else
        {
            result = loggedOut.getList(tmp);
        }

        Collections.sort((List<Item>)((Object) result));
        return result;
    }

    public double getRating()
    {
        return rating;
    }

    public void setRating(double rating)
    {
        this.rating = rating;
    }

    public int compareTo(Listable l) 
    {
        Item i = (Item) l;
        if(i.getRating() > getRating())
        {
            return 1;
        }
        else if (i.getRating() < getRating())
        {
            return -1;
        }
        return 0;
    }


	// Get methods used in JSP
	public String getID()          { return itemID; }
	public String getName()        { return itemName; }
	public String getURL()         { return itemURL; }
	public String getPrice()       { return itemPrice; }
	public String getStock()       { return itemStock; }
	public String getDescription() { return itemDescription; }
}
