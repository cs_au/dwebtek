package skeen;

import skeen.*;
import java.util.*;
import org.jdom.Element;
import org.jdom.Namespace;

public class InverseItemSort extends SortItems
{
    public InverseItemSort()
    {
        super();
    }
    
    public InverseItemSort(SortItems i)
    {
        super(i);
    }
    
    public List<Listable> getList(List<Element> tmp)
    {
        List<Listable> result = next.getList(tmp);

        for(Listable l : result)
        {
            Item i = (Item) l;
            i.setRating(-i.getRating());
        }
        
		return result;
    }

    public List<Listable> getList(List<Element> tmp, String a)
    {
        return getList(tmp);
    }
}
