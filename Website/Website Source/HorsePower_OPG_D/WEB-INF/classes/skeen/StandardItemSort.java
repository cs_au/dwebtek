package skeen;

import skeen.*;
import java.util.*;
import org.jdom.Element;
import org.jdom.Namespace;

public class StandardItemSort extends SortItems
{
    public StandardItemSort()
    {
        super();
    }

    public StandardItemSort(SortItems i)
    {
        super(i);
    }

    public List<Listable> getList(List<Element> tmp)
    {
        List<Listable> result = next.getList(tmp);

        int counter = 0;
	    for(Listable l : result)
        {
            Item i = (Item) l;
            i.setRating(counter--);
        }

		return result;
    }

    public List<Listable> getList(List<Element> tmp, String a)
    {
        return getList(tmp);
    }
}
