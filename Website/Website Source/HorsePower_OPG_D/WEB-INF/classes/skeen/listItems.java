package skeen;

import skeen.*;
import java.util.*;
import org.jdom.Element;
import org.jdom.Namespace;

public class listItems extends SortItems
{
    public listItems()
    {
        super(null);
    }

    public List<Listable> getList(List<Element> tmp)
    {
        List<Listable> result = new ArrayList<Listable>();

        Iterator iterator = tmp.iterator();
		
		while (iterator.hasNext())
		{
			Element e = (Element)iterator.next();
			String id =          e.getChildText("itemID", w);
			String name =        e.getChildText("itemName", w);
			String url =         e.getChildText("itemURL", w);
			String price =       e.getChildText("itemPrice", w);
			String stock =       e.getChildText("itemStock", w);
			String description = e.getChildText("itemDescription", w);
			//String description = (new XMLOutputter()).outputString(e.getChild("itemDescription", w));
            //TODO: Tager <w:itemdescription> med, fix det, og brug s�
            //overst�ende
			
			Item item = new Item(textEscape.forHTML(id),
                                 textEscape.forHTML(name),
                                 textEscape.forHTML(url),
                                 textEscape.forHTML(price),
                                 textEscape.forHTML(stock),
                                 description);
			result.add(item);
		}
		
		return result;
    }

    public List<Listable> getList(List<Element> tmp, String a)
    {
        return getList(tmp);
    }
}
