package skeen;

public class BadUrlResponse extends Exception
{
    private int responseCode = -1;
    
    public BadUrlResponse(String responseCode)
    {
        super(responseCode);
        try
        {
            this.responseCode = Integer.parseInt(responseCode);
        }
        catch (Exception e)
        {
			mail.sendDevs("Unparseable data at BadUrlResponse!"); 
        }
    }
    
    public int getResponseCode()
    {
        return responseCode;
    }
}
