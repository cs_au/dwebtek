package skeen;

import java.net.*;
import java.io.*;
import java.util.regex.*;
import java.util.concurrent.*;
import java.util.Arrays;

public class GoogleStats extends Thread
{
    private Semaphore Mutex;
    private String our, their;
    private double Rating = -1;

    public GoogleStats(String our, String their)
    {
        Mutex = new Semaphore(1);
        this.our = our;
        this.their = their;
    }

    public void run()
    {
        try
        {
            Mutex.acquire();
            Rating = getGoogleRating(our, their);
        }
        catch(Exception e)
        {
        }
        finally
        {
            Mutex.release();
        }
    }

    public double getRating()
    {
        double returnVar = -1;
        try
        {
            Mutex.acquire();
            returnVar = Rating;
        }
        catch(Exception e)
        {
        }
        finally
        {        
            Mutex.release();
        }
        return returnVar;
    }

    private String remove_punctation(String input)
    {
        String[] strings = input.split("\\.");
        String output = "";
        for(String s : strings)
            output = output.concat(s);
        return output;
    }

    private String remove_wrapper(String input)
    {
         String regex = "(((\\d)*(\\.))*(\\d)+)";
         Pattern patt = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
         Matcher matcher = patt.matcher(input);
         if(matcher.find())
         {
             return matcher.group(0);
         }
         else
         {
             return null;
         }   
    }

    private long getGoogleResults(String search)
    {
        try 
        {
            String req = "http://www.google.dk/search?"+
                "q="+URLEncoder.encode(search, "UTF8");

            System.out.println(req);

            HttpURLConnection con = 
                (HttpURLConnection) (new URL(req)).openConnection();

            con.setRequestProperty("User-Agent", "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)");
            con.setInstanceFollowRedirects(false);

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String strLine;
            while ((strLine = in.readLine()) != null)
            {
                System.out.println(strLine);
                String regex = "Ca. (((\\d)*(\\.))*(\\d)+) resultater";
                Pattern patt = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
                Matcher matcher = patt.matcher(strLine);
                if (matcher.find())
                {   
                    long returnval = Long.parseLong(remove_punctation(remove_wrapper(matcher.group(0))));
                    return returnval;
                }
            }
        }
        catch (IOException e) 
        {
            System.err.println(e); 
        }
        return 0;
    }

    /* Gets the rating of which strings er related.
     * This version probes google to do this.
     *  
     * param(our):      String, containing the first search string (our object)
     * param(their):    String, containing the Second search string (their object)
     * ret:             double, containing the rating
     *
     * Algorithm: Rating = Result(our+their)/(Result(our)+Result(their))
     */
    private double getGoogleRating(String our, String their)
    {
        return (double)getGoogleResults("\"" + our + "\"" + " " + "\"" + their + "\"")
            / (((double)getGoogleResults("\"" + our + "\"")) + /*((double)getGoogleResults("\"" + their + "\""))*/); //Sverre: fjernede their i n�vneren. Nu er forholdet mere lig: antal f�lles resultater pr. vores item. Dvs. returnerer h�jt hvis vores item er meget relateret til deres item, og ikke s� meget hvis deres item er meget relateret til vores item. Husk at normalisere, dog.
        /*
        double lol = (double)getGoogleResults("\"" + our + "\"" + " " + "\"" + their + "\"")
            / (((double)getGoogleResults("\"" + our + "\"")) + ((double)getGoogleResults("\"" + their + "\"")));
        return -Math.log10(lol);
        */
    }
}


