import java.io.*;
import java.net.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import org.jdom.*;
import org.jdom.input.*;
import org.jdom.output.*;
import org.jdom.transform.*;
import skeen.*;

abstract class ItemServletHelper extends HttpServlet
{
    protected static String shopKey;
    public static final Namespace w = Namespace.getNamespace("http://www.cs.au.dk/dWebTek/2011");
    
    protected String itemID;
    protected String itemName;
    protected String itemPrice;
    protected String itemURL;
    protected String itemDescription;
    protected String itemStockAdjustment;
            
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		doPost(request, response);
	}
    
    protected HttpServletRequest request;
    protected HttpServletResponse response;
    protected HttpSession session;
    protected String previousPage;
	public void doPost(HttpServletRequest request, HttpServletResponse response)
                                        throws ServletException, IOException
    {
        this.request = request;
        this.response = response;
        session = request.getSession();
        previousPage = request.getParameter("mainPage");
        shopKey = getServletContext().getInitParameter("shopKey");
        
		// Remove the items bean for updates.
		session.removeAttribute("items");
    }
    
    protected Document createItemRequest()
    {
        String initParameter = getServletContext().getInitParameter("createItemRequest-xml");
        if (initParameter == null) mail.sendDevs("Web.xml - initParameter not well defined! - at createItemRequest");
		Document res = null;
		try
		{   /* Build a new Document object from the createItemRequest.xml file. */
			res = new SAXBuilder().build(new File(getServletContext().getRealPath(initParameter)));
			res.getRootElement().getChild("shopKey", w).setText(shopKey);
			res.getRootElement().getChild("itemName", w).setText(itemName);
		}
		catch(Exception e)
		{            
            mail.sendDevs("Battle Steed. - at CreateItemRequest");
		}
        return res;
    }
    
    protected Document modifyItemRequest()
    {
        String initParameter = getServletContext().getInitParameter("modifyItemRequest-xml");
        if(initParameter == null) mail.sendDevs("Web.xml - modifyItemRequest.initParameter not well defined! - at ModifyItemRequest");
        Document res = null;
        try
        {
            res = new SAXBuilder().build(new File(getServletContext().getRealPath(initParameter)));
            res.getRootElement().getChild("shopKey", w).setText(shopKey);
            res.getRootElement().getChild("itemID", w).setText(itemID);
            res.getRootElement().getChild("itemName", w).setText(itemName);
            res.getRootElement().getChild("itemPrice", w).setText(itemPrice);
            res.getRootElement().getChild("itemURL", w).setText(itemURL);
            res.getRootElement().getChild("itemDescription", w).setText(itemDescription);
        }
        catch(Exception e)
        {
            mail.sendDevs("Horse! - at ModifyItemRequest");
        }
        return res;
    }
    
    protected Document modifyItemStockRequest()
    {
        String initParameter = getServletContext().getInitParameter("adjustItemStockRequest-xml");
        if(initParameter == null) mail.sendDevs("Web.xml - adjustItemStock.initParameter not well defined! - at ModifyItemRequest");            
        Document res = null;
        try
        {   // Build a new Document object from the adjustItemStockRequest.xml file. 
            res = new SAXBuilder().build(new File(getServletContext().getRealPath(initParameter)));
            res.getRootElement().getChild("shopKey", w).setText(shopKey);
            res.getRootElement().getChild("itemID", w).setText(itemID);
            res.getRootElement().getChild("adjustment", w).setText(itemStockAdjustment);
        }
        catch (Exception e)
        {
            mail.sendDevs("Swaybacked Horse. - at ModifyItemStockRequest");
        }
        return res;
    }
    
    
 /* // The enum type Request contains a constructor, and each enum constant is declared with
    // parameters to be passed to the constructor when it is created.
    private enum Request{
        MODIFY      (modifyItemRequest()),
        CREATE      (createItemRequest()),
        MODIFYSTOCK (modifyItemStockRequest());
        
        private Document docRequest;
        Request(Document docRequest)
        {
            this.docRequest = docRequest;
        }
        public Document getRequestDocument()
        {
            return docRequest;
        }
        public void getValidatedRequestDocument()
        {
            validateDoc(docRequest);
        }
    } */
    
    /* // This could be changed to use pipedStreams instead of using string as mediator
    
    public static Document validator(Document requestDoc) throws ValidationException
    {
        return (validateDoc(requestDoc)?requestDoc:null);
    }    
    
    protected static boolean validateDoc(Document docToBeValidated) throws ValidationException
    {
        return validateXML(
            new org.xml.sax.InputSource(
                new StringReader(
                    (new XMLOutputter()).outputString(
                        docToBeValidated
                    )
                )
            )
        );
    }
    
    
    private static boolean validateXML(org.xml.sax.InputSource in) throws ValidationException
    {
        boolean validated = true;
        try
        {
            SAXBuilder b = new SAXBuilder();
            
            //validation should occur.
            b.setValidation(true); 
            
            // Allow validation against XML Schema descriptions (referenced in the XML-file).
            b.setProperty(
                "http://java.sun.com/xml/jaxp/properties/schemaLanguage",
                "http://www.w3.org/2001/XMLSchema");
            
            try
            {
                Document d = b.build(in);
            }
            catch(JDOMParseException e)
            {
                validated = false;
                throw new ValidationException(e.getMessage());
            }
        }
        catch(Exception e)
        {
            validated = false;
            throw new ValidationException("" + e);
        }
        return validated;
    } */
    
    protected static boolean isNullOrBlank(String s)
    {
        return (s==null || s.trim().equals(""));
    }
    
    // abstract method for writing short errormessage
    // PLEASE use redirectBackWithMessage in the implementation
    abstract protected void error(String message);
    
    // @precondition errormessage and anchorlink holds valid URLencoded strings
    protected void redirectBackWithMessage(String errortype,String errormessage,String anchorlink)
    {
        try
        {
            response.sendRedirect(response.encodeRedirectURL(
                previousPage +
                (isNullOrBlank(errortype)?"":
                    "?"+errortype+"=" + URLEncoder.encode(errormessage, "UTF-8")) +
                (isNullOrBlank(anchorlink)?"":
                    "#"+anchorlink)
            ));
        }
        catch (IOException e)
        {
            mail.sendDevs("Redirect IOException. " +"Thrown from: ItemServletHelper.");	
        }
    }
    
    protected void succesRedirect()
    {
        redirectBackWithMessage("","","item="+itemID);
    }
}