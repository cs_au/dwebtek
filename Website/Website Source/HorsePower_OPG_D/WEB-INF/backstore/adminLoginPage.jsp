<% session.setAttribute("mainPage", "adminLoginPage"); %>

<jsp:include page="/WEB-INF/modules/header.jsp"/>

	<h1>Administrator login</h1>
	<jsp:include page="/WEB-INF/modules/adminLogin.jsp"/>

<jsp:include page="/WEB-INF/modules/footer.jsp"/>
