<% session.setAttribute("mainPage", "backstore"); %>

<jsp:include page="/WEB-INF/modules/header.jsp"/>

	<h1>Create New Items</h1>
	<jsp:include page="/WEB-INF/modules/createItem.jsp"/>
	<h1>Modify Items</h1>
	<jsp:include page="/WEB-INF/modules/modifyItem.jsp"/>

<jsp:include page="/WEB-INF/modules/footer.jsp"/>
