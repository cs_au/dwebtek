The general process of a URI-request to the webshop depends upon the URI and can be expressed by the following diagram: 
\begin{center}
    \includegraphics[width=6.5in]{diagram1}
\end{center}

\subsubsection{Controller}
If the URI is not specified in web.xml to be mapped to a specific servlet, the Controller servlet is invoked. 
\begin{center}
    \includegraphics[width=5in]{diagram2}
\end{center}

The Controller looks (using JDOM) through pages.xml [1] for a $page$-element
with a $path$-value that corresponds to the given URI. If such an element is found, the Controller creates some JavaBeans (Item, Shop, Cart, Sale, Customer, which all implement the custom Listable interface) and stores them in session attributes [2] - that is, if they don't already exist. These JavaBeans contain data from the cloud in simple Lists. However, if such an element is not found, the client is redirected to an error page (pageofshit.jsp) [3]. 

When the Controller has created the needed JavaBeans, the $link$-value of the
$page$-element is used to redirect the client to the relevant JSP-page (within the /backstore or /frontstore folders, see section 2.1 for more details). 

\subsubsection{Servlet}
If the URI is specified in web.xml, it is mapped to a specific servlet (e.g. AddServlet, LoginServlet, hence the *) which operates independantly of the Controller servlet. 
\begin{center}
    \includegraphics[width=5in]{diagram3}
\end{center}
The servlet will register the $mainPage$-attribute which is declared on every page redirected to by the Controller, that is, all pages in the /frontstore or /backstore folders. This is a hack made necessary by our otherwise splendid modular structure (see section 2.1) - if this attribute is not used for redirects, the servlets will try to redirect to the "page" from which the client issued the request. However, this "page" is actually a module, which we will explain in further detail in section 2.1. 

After sanity checks (validation of the form data) [1] the client is either redirected back to the mainPage page with an error variable in the URI (e.g. ".../login?loginerror=Missing Argument") [2], which is caught by JSTL if-statements in the JSP-module (thus only shown if the variable is defined). 

If the form data passes the sanity checks, a JDOM Document object is created (using SAXBuilder). This Document object is created using "empty" XML templates (e.g. <w:customerName/>), which are then filled out using the form data. Then it is sent as a cloud request [3], and if the cloud (the class, see section 2.2) throws an exception, it is caught and an error is issued in the same way as with failed sanity checks [4]. Otherwise, some session attributes are declared and the redirect is performed without error variables [5]. 
