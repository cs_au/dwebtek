<% session.setAttribute("mainPage", "pageofshit"); %>

<jsp:include page="/WEB-INF/modules/header.jsp"/>

		<h1>Error 404</h1>
		<p>What the heck do you think you're doing here? Get out! This is for cool people only!</p>
		<p>Actually, this page doesn't exist in real life (nor in cyberspace), so there's a good chance you either took a wrong turn somewhere, or (more likely) messed something up in your typing. You silly goose.</p>
		<p>To help you improve your typing and avoid this situation again in the future, we advise you to visit <a href="http://www.splashesfromtheriver.com/spelling/courseoutline.htm">this page</a>. Good luck!</p>
		<p>In the (very unlikely) situation that <i>we</i> messed something up, please visit our <a href="contact">Contact</a> page to let us know that your typing is perfectly fine, and that you're certain the page you tried to access really exists (either in real life or in cyberspace).</p>

<jsp:include page="/WEB-INF/modules/footer.jsp"/>