<% session.setAttribute("mainPage", "itembought"); %>
<jsp:include page="/WEB-INF/modules/header.jsp"/>

<ul>
    <c:if test="${param.succescode  != null}"><span class="error">${param.succescode }</span><br/><br/></c:if>
</ul>

	<h1>Congratulations!</h1>
    <p>You're now the proud owner of:<br/>
    <ul>
        <c:if test="${param.items != null}"><span>${param.items}</span><br/><br/></c:if>
    </ul>
    <a href="home">Snazzy</a>, eh?</p>

<jsp:include page="/WEB-INF/modules/footer.jsp"/>
