<% session.setAttribute("mainPage", "frontpage"); %>

<jsp:include page="/WEB-INF/modules/header.jsp"/>

<jsp:include page="/WEB-INF/modules/login.jsp"/>

	<h1>Greetings!</h1>
	<div id="items">
	<p>Welcome to the <b>horsest</b> webshop in the world! We don't sell horses, nor do we sell power(s) - for such items, please visit your local stables, your electricity provider, or a science lab that works with radioactive materials, respectively.</p>
	<p>The HorsePower Webshop specializes in different types of <b>food</b>. Our specialty is burgers, but we our product line ranges from simple mayo burgers to exotic foods like plain white bread and lemons. These products are very rare, however, so make sure to get them while they're still in stock.</p>
	<p>Have a click on the Products menu item (or right <a href="products">here</a>) for a full list of our entire product line. To purchase the nutritional goodies, make sure you're logged in. Don't have an account? It's as easy as 1, 2, 3 (and 4), so <a href="newcustomer">go ahead</a> if you're hungry!</p>
	<p>The HorsePower Webshop is part of a splendid <b>shopping network</b>, so if you're not hungry or just don't feel like buying the most amazing food in the world right now, go ahead and have a look at some of the other shops in the network. You can use the "shop-down drop-down" below, or you can go to our <a href="shops">Other Shops</a> page for a full list.</p>
	
	<jsp:include page="/WEB-INF/modules/shops.jsp"/>
	
	<br/><span class="small">Disclaimer: The HorsePower Webshop cannot guarantee the delivery of the products presented on this website. Any products ordered will be placed on a small bench outside the web master's house, and we rely on friendly souls to stop by, pick up, and then deliver the package to the rightful recipient. If a package is removed from the bench, the C4-explosives planted inside will destroy the package after 2 hours, should the iris-scanner not recognize the person handling the package. These security-measures are the reason for the "Shipping and Handling" fee of 5.000 kr.</span>
	</div>

<jsp:include page="/WEB-INF/modules/footer.jsp"/>
