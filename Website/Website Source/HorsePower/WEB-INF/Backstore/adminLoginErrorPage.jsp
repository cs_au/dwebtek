<%@ page import="java.net.URLEncoder" %>
<% 
    session.setAttribute("mainPage", "adminLoginErrorPage");  
    response.sendRedirect(response.encodeRedirectURL(request.getParameter("mainPage") +"?adminloginerror=" + URLEncoder.encode("Please enter valid login data!", "UTF-8")));
%>
