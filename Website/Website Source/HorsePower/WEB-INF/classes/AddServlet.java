import java.io.IOException;
import java.net.URLEncoder;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.jdom.Document;
import skeen.*;

public class AddServlet extends HttpServlet
{
    public void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException
    {
        HttpSession session = request.getSession();
        String previousPage = request.getParameter("mainPage");
		
        Document sellItemsRequest = null;

		String itemID  = request.getParameter("itemID");
		String itemName = request.getParameter("itemName");
		String itemURL = request.getParameter("itemURL");
		String itemPrice = request.getParameter("itemPrice");
		String itemStock = request.getParameter("itemStock");
		String itemDescription = request.getParameter("itemDescription");

        String saleAmount = request.getParameter("saleAmount");

        int itemStock_Integer = 0;
        int saleAmount_Integer = 0;
        
        try
        {
            itemStock_Integer = Integer.parseInt(itemStock);
            saleAmount_Integer = Integer.parseInt(saleAmount);
        }
        catch(NumberFormatException e)
        {
			mail.sendDevs("Unparseable data at AddServlet!");
        }

        if(itemID==null || itemName==null || itemURL==null || itemPrice==null || itemStock==null || itemDescription==null || saleAmount==null)
        {
             response.sendRedirect(response.encodeRedirectURL(previousPage + "?addToCartError=" + URLEncoder.encode("Missing Argument", "UTF-8")));
             return;
        }
        else if(saleAmount_Integer < 1)
        {
            response.sendRedirect(response.encodeRedirectURL(previousPage + "?addToCartError=" + URLEncoder.encode("saleAmount should be a positive number", "UTF-8")));
            return;
        }
        
        @SuppressWarnings("unchecked")
        List<Item> cart = (List<Item>) session.getAttribute("cart");
        Item cur = null;
	    for(Item i : cart)
        {
            if(i.getID().equals(itemID))
            {
                cur = i;
            }
        }

        if(cur==null)
        {
            if(itemStock_Integer<saleAmount_Integer)
            {
                response.sendRedirect(response.encodeRedirectURL(previousPage + "?addToCartError=" + URLEncoder.encode("You can't buy more items then there's currently in stock", "UTF-8")));
                return;
            }
            else
            {
                //NOTE: saleAmount is intentionally placed here
                cart.add(new Item(textEscape.forHTML(itemID),
                                  textEscape.forHTML(itemName),
                                  textEscape.forHTML(itemURL),
                                  textEscape.forHTML(itemPrice),
                                  textEscape.forHTML(saleAmount),
                                  textEscape.forHTML(itemDescription)));
            }
        }
        else
        {
            int old_saleAmount = Integer.parseInt(cur.getStock());
            int total_saleAmount = (saleAmount_Integer+old_saleAmount);
            if(itemStock_Integer < total_saleAmount)
            {
                response.sendRedirect(response.encodeRedirectURL(previousPage + "?addToCartError=" + URLEncoder.encode("You can't buy more items then there's currently in stock", "UTF-8")));
                return;
            }
            else
            {
                //NOTE: saleAmount is intentionally placed here
                cart.add(new Item(textEscape.forHTML(itemID),
                                  textEscape.forHTML(itemName),
                                  textEscape.forHTML(itemURL),
                                  textEscape.forHTML(itemPrice),
                                  textEscape.forHTML("" + total_saleAmount),
                                  textEscape.forHTML(itemDescription)));
                cart.remove(cur);
            }
        }

        session.setAttribute("cart", cart);
        response.sendRedirect(response.encodeRedirectURL(previousPage));
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        doPost(request, response);
    }
}

