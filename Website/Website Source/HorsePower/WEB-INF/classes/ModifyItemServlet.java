import java.io.*;
import java.net.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import org.jdom.*;
import org.jdom.input.*;
import org.jdom.output.*;
import org.jdom.transform.*;

import skeen.*;

public class ModifyItemServlet extends ItemServletHelper
{    
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        super.doPost(request, response);
        
        itemID = request.getParameter("itemID");
        itemName = request.getParameter("itemName");
        itemPrice = request.getParameter("itemPrice");
        itemURL = request.getParameter("itemURL");
        itemDescription = request.getParameter("itemDescription");
        
        String itemStockAdjusted = request.getParameter("itemStockAdjusted");
        String itemStockOriginal = request.getParameter("itemStockOriginal");
		
        // hidden fields to check if only necessary to send 1 of modifyItemRequest and modifyItemStockRequest
        // check for any changed fields
        boolean isModifyRequestNecessary = (
                /*isItemNameChanged*/         ! itemName.equals         (request.getParameter("itemNameOriginal"))        ||
                /*isItemPriceChanged*/        ! itemPrice.equals        (request.getParameter("itemPriceOriginal"))       ||
                /*isItemURLChanged*/          ! itemURL.equals          (request.getParameter("itemURLOriginal"))         ||
                /*isItemDescriptionChanged*/  ! itemDescription.equals  (request.getParameter("itemDescriptionOriginal"))
            );
        boolean isModifyStockNecessary = (
                /*isItemStockChanged*/        ! (itemDescription.equals (itemStockOriginal))
            );
        
		//sanity checks, itemURL may be blank.
		if(!itemURL.equals(""))
		{
			try
			{
				URL url = new URL(itemURL);
				URLConnection connection = url.openConnection();
				connection.connect();
			} 
			catch (Exception e) 
			{
                error("itemURL was not found, or not valid.");
				return;
			}
		}
        if(isNullOrBlank(itemName) || isNullOrBlank(itemID) || isNullOrBlank(itemPrice) || isNullOrBlank(itemDescription) || isNullOrBlank(itemStockAdjusted) || isNullOrBlank(itemStockOriginal))
        {
            error("Missing Arguments" +
                "- Please make sure the form is filled before submitting.");
            return;
        }
        
        // modify item details
        if (isModifyRequestNecessary == true)
        {
            try
            {   //ignored response document (because it is not defined according to the project specifications)
                cloud.request("modifyItem", modifyItemRequest());
            }
            catch(BadUrlResponse e)
            {
                if (e.getResponseCode() == HttpURLConnection.HTTP_BAD_REQUEST )
                {
                    error("Could not modify item!");
                    return;
                }
                else
                {
                    mail.sendDevs("Bad response codeerror. " + "Thrown from: ModifyItemServlet. Value: " + e.getResponseCode());
                }
            }
            catch(ValidationException e)
            {
                
                Format format = Format.getPrettyFormat();
                format.setEncoding("UTF-8");
                XMLOutputter out = new XMLOutputter(format);
                
                String statementWhatWentWrong = e.getValidationResponseCode();
                error("Data failed validation! ERROR: " + statementWhatWentWrong +
                    "_____" + out.outputString(modifyItemRequest()));
                
                return;
            }
        }        
        
        // Adjust item stock
        if (isModifyStockNecessary == true)
        {
            //calculated adjustment of itemStock
            try
            {
                itemStockAdjustment = "" + (Integer.parseInt(itemStockAdjusted) - Integer.parseInt(itemStockOriginal));
            }
            catch (NumberFormatException e)
            {
                error("Please enter valid nonnegative Integer as stock!");
                return;
            }
            
            // send the adjustItemStockRequest to the cloud
            try
            {
                cloud.request("adjustItemStock", modifyItemStockRequest());
            }
            catch(BadUrlResponse e)
            {
                if (e.getResponseCode() == HttpURLConnection.HTTP_BAD_REQUEST )
                {
                    error("Unable to adjust stock!");
                }
                else
                {
                    mail.sendDevs("UnableToOpenUrl. " + "Thrown from: ModifyItemServlet:adjustItemStock. Value: " + e.getResponseCode());
                }
            }
            catch(ValidationException e)
            {
                String statementWhatWentWrong = e.getValidationResponseCode();
                error("Data failed validation! ERROR: " + statementWhatWentWrong);
                return;
            }
        }
        succesRedirect();
    }
    
    protected void error( String message)
    {
        redirectBackWithMessage(
            "item="+itemID +"&"+ "modifyitemerror",
            message,
            "item=" + itemID
        );
    }
}
