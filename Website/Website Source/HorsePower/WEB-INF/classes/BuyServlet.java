import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.jdom.Document;
import org.jdom.Namespace;
import org.jdom.input.SAXBuilder;
import skeen.BadUrlResponse;
import skeen.*;

public class BuyServlet extends HttpServlet
{
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String shopKey = getServletContext().getInitParameter("shopKey");
        Namespace w = Namespace.getNamespace("http://www.cs.au.dk/dWebTek/2011");

        List<String> errors = new ArrayList<String>();
        List<String> items = new ArrayList<String>();

        HttpSession session = request.getSession();
        String previousPage = request.getParameter("mainPage");

        Document sellItemsRequest = null;
        String customerID = (String) session.getAttribute("customerID");

        if(customerID==null)
        {
            response.sendRedirect(response.encodeRedirectURL(previousPage + "?buyerror=" + URLEncoder.encode("Missing Argument", "UTF-8")));
            return;
        }

        @SuppressWarnings("unchecked")
        List<Item> cart = (List<Item>) session.getAttribute("cart");
        for(Item i : cart)
        {        

            String initParameter = getServletContext().getInitParameter("sellItemsRequest-xml");
            if(initParameter == null)
                mail.sendDevs("Web.xml - initParameter not well defined! - at BuyServlet");
            try
            {
                /* Build a new Document object from the sellItemsRequest.xml file. */
                sellItemsRequest = new SAXBuilder().build(new File(getServletContext().getRealPath(initParameter)));
                sellItemsRequest.getRootElement().getChild("shopKey", w).setText(shopKey);
                sellItemsRequest.getRootElement().getChild("itemID", w).setText(i.getID());
                sellItemsRequest.getRootElement().getChild("customerID", w).setText(customerID);
                sellItemsRequest.getRootElement().getChild("saleAmount", w).setText(i.getStock());
            }
            catch(Exception e)
            {
                mail.sendDevs("FileNotFound - " + initParameter + " - requested at BuyServlet!");
            }

            Document sellItemResponse = null;
            try
            {
                sellItemResponse = cloud.request("sellItems", sellItemsRequest);
            }
            catch(BadUrlResponse e)
            {
                /*
                if (e.getResponseCode() == HttpURLConnection.HTTP_BAD_REQUEST )
                {
                    response.sendRedirect(response.encodeRedirectURL(previousPage + "?buyerror=" + URLEncoder.encode("Bad Request: There was an issue checking out: " + i.getName(), "UTF-8")));
                    return;
                }
                else
                {
                   mail.sendDevs("Wierd cloud response - at BuyServlet!");
                }
                */
            }
            catch(Exception e)
            {
            }

            if(sellItemResponse!=null)
            {
                String itemSoldOut = sellItemResponse.getRootElement().getChildText("itemSoldOut", w);
                String error = sellItemResponse.getRootElement().getChildText("error", w);

                if(itemSoldOut!=null)
                {
                    errors.add(i.getName() + " was sadly soldOut");
                }
                else if(error!=null)
                {
                    errors.add(i.getName() + " triggered an error");
                }
                else
                {
                    items.add(i.getStock() + "x\t" + i.getName());
                }
            }
            else
            {
                /*
                   String recv = getServletContext().getInitParameter("email-devs");
                   String[] to = recv.split(" ");
                   mail.send(to, "", "SellItemResponse", "Thrown from: BuyServlet. SellItemResponse is null");
                //return;
                */
            }
        }
		// Remove the items bean for updates.
		session.removeAttribute("items");
		// Clear the cart.
        session.setAttribute("cart", new ArrayList());

        String buyerror = "";
        for(String p : errors)
        {
            buyerror = buyerror.concat("<li>" + p + "</li>");
        }

        String itemsBought = "";
        for(String p : items)
        {
            itemsBought = itemsBought.concat("<li>" + p + "</li>");
        }

        response.sendRedirect(response.encodeRedirectURL("itembought?succescode=" + URLEncoder.encode(buyerror, "UTF-8") + "&" + "items=" + URLEncoder.encode(itemsBought, "UTF-8")));
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        doPost(request, response);
    }
}

