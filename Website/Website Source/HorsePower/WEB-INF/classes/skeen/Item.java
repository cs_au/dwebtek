package skeen;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.Namespace;
import org.jdom.output.XMLOutputter;
import skeen.*;
import javax.servlet.ServletContext;

public class Item implements Listable
{
	private static Namespace w = Namespace.getNamespace("http://www.cs.au.dk/dWebTek/2011");

	private String itemID, itemName, itemURL, itemPrice, itemStock, itemDescription;

	// Constructor
	public Item(String itemID, String itemName, String itemURL, String itemPrice, String itemStock, String itemDescription)
	{
		this.itemID = itemID;
		this.itemName = itemName;
		this.itemURL = itemURL;
		this.itemPrice = itemPrice;
		this.itemStock = itemStock;
		this.itemDescription = itemDescription;
	}
	
	// Empty constructor
	public Item() 
    {
    }
	
	// Returns the request string
	public String getRequestString()
	{
        /*
        String shopID = con.getInitParameter("shopID");
        if(shopID == null)
            mail.sendDevs("Web.xml - shopID not well defined! - at Item");
        */
		return "listItems?shopID=74";
	}
	
	// Returns a list of Item objects
	public List<Listable> generateMatches(Document doc)
	{
		List<Listable> result = new ArrayList<Listable>();
        if (doc == null) return result;
        List<Element> tmp = doc.getRootElement().getChildren("item", w);
        if (tmp == null) return result;
        
        Iterator iterator = tmp.iterator();
		
		while (iterator.hasNext())
		{
			Element e = (Element)iterator.next();
			String id =          e.getChildText("itemID", w);
			String name =        e.getChildText("itemName", w);
			String url =         e.getChildText("itemURL", w);
			String price =       e.getChildText("itemPrice", w);
			String stock =       e.getChildText("itemStock", w);
			String description = e.getChildText("itemDescription", w);
			//String description = (new XMLOutputter()).outputString(e.getChild("itemDescription", w));
            //TODO: Tager <w:itemdescription> med, fix det, og brug s�
            //overst�ende
			
			Item item = new Item(textEscape.forHTML(id),
                                 textEscape.forHTML(name),
                                 textEscape.forHTML(url),
                                 textEscape.forHTML(price),
                                 textEscape.forHTML(stock),
                                 description);
			result.add(item);
		}
		
		return result;
	}
	
	// Get methods used in JSP
	public String getID()          { return itemID; }
	public String getName()        { return itemName; }
	public String getURL()         { return itemURL; }
	public String getPrice()       { return itemPrice; }
	public String getStock()       { return itemStock; }
	public String getDescription() { return itemDescription; }
}
