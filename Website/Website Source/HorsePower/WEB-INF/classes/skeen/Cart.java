package skeen;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpSession;
import org.jdom.Document;

public class Cart implements Listable
{
    //NOTE: The itemStock is being used to hold itemAmount in the cart
    private HttpSession session;
	
	public Cart(HttpSession session)
	{
        this.session = session;
        
        @SuppressWarnings("unchecked")
        List<Listable> temp = (List<Listable>) session.getAttribute("cart");
		if(temp == null)
		{
			session.setAttribute("cart", new ArrayList<Listable>());
		}
    }
	
	// Returns the request string
	public String getRequestString()
	{
		return "";
	}
    
	// Returns a list of Item objects
	public List<Listable> generateMatches(Document doc)
	{
        @SuppressWarnings("unchecked")
        List<Listable> temp = (List<Listable>) session.getAttribute("cart");
		return temp;
	}
}
