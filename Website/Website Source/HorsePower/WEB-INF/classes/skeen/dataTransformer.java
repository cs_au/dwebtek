package skeen;

import skeen.*;
import java.util.*;
import javax.servlet.http.HttpSession;

public class dataTransformer
{
    private dataTransformer()
    {
    }

    private static HttpSession session = null;
    public static void seedSession(HttpSession s)
    {
        session = s;
    }

    @SuppressWarnings("unchecked")
    private static List<pair<Integer, String>> convert(Map<String, Integer> input)
    {
        List<pair<Integer, String>> data = new ArrayList<pair<Integer, String>>();
        Iterator mapIterator = input.entrySet().iterator();
        while (mapIterator.hasNext()) 
        {
            Map.Entry pairs = (Map.Entry)mapIterator.next();
            data.add(new pair(pairs.getValue(), pairs.getKey()));
        }
        return data;
    }

    public static List<pair<Integer, String>> itemID_to_ItemName(List<pair<Integer, String>> input)
    {
        Model sessionData = (Model) session.getAttribute("items");
        if(sessionData==null)  
        {
            mail.sendDevs("Error getting data at dataTransformer.java");
            return null;
        }
        List<Listable> inputData=sessionData.getMatches();
        if(inputData.isEmpty())
        {
            mail.sendDevs("Empty input data at dataTransformer.java");
            return null;
        }
        
        for(Listable l : inputData)
        {
            Item i = (Item) l;
            for(int x=0; x<input.size(); x++)
            {
                if(i.getID().equals(input.get(x).get2()))
                {
                    input.get(x).set2(i.getName());
                }
            }
        }
        return input;
    }

    public static List<pair<Integer, String>> customerID_to_customerName(List<pair<Integer, String>> input)
    {
        Model sessionData = (Model) session.getAttribute("customers");
        if(sessionData==null)  
        {
            mail.sendDevs("Error getting data at dataTransformer.java");
            return null;
        }
        List<Listable> inputData=sessionData.getMatches();
        if(inputData.isEmpty())
        {
            mail.sendDevs("Empty input data at dataTransformer.java");
            return null;
        }
        
        for(Listable l : inputData)
        {
            Customer i = (Customer) l;
            for(int x=0; x<input.size(); x++)
            {
                if(i.getID().equals(input.get(x).get2()))
                {
                    input.get(x).set2(i.getName());
                }
            }
        }
        return input;
    }

    public static List<pair<Integer, String>> sales_by_itemID_transform(List<Listable> input)
    {
        Map<String, Integer> map = new TreeMap<String, Integer>();

        for(Listable l : input)
        {
            Sale s = (Sale) l;

            int saleAmount=0;
            try
            {
                saleAmount = Integer.parseInt(s.getSaleAmount());
            }
            catch(Exception e)
            {
                mail.sendDevs("Unparsable data in dataTransformer.java");
            }

            Integer num = map.get(s.getItemID());
            if(num==null)
            {
                map.put(s.getItemID(), saleAmount);
            }
            else
            {
                map.put(s.getItemID(), saleAmount+num);
            }
        }
        return convert(map);
    }

    public static List<pair<Integer, String>> cash_by_itemID_transform(List<Listable> input)
    {
        Map<String, Integer> map = new TreeMap<String, Integer>();

        for(Listable l : input)
        {
            Sale s = (Sale) l;

            int saleAmount=0;
            int saleItemPrice=0;
            try
            {
                saleAmount = Integer.parseInt(s.getSaleAmount());
                saleItemPrice=Integer.parseInt(s.getSaleItemPrice());
            }
            catch(Exception e)
            {
                mail.sendDevs("Unparsable data in dataTransformer.java");
            }

            Integer priceSoFar = map.get(s.getItemID());
            if(priceSoFar==null)
            {
                map.put(s.getItemID(), saleAmount*saleItemPrice);
            }
            else
            {
                map.put(s.getItemID(), priceSoFar+(saleAmount*saleItemPrice));
            }
        }
        return convert(map);
    }

        public static List<pair<Integer, String>> sales_by_customerID_transform(List<Listable> input)
    {
        Map<String, Integer> map = new TreeMap<String, Integer>();

        for(Listable l : input)
        {
            Sale s = (Sale) l;

            int saleAmount=0;
            try
            {
                saleAmount = Integer.parseInt(s.getSaleAmount());
            }
            catch(Exception e)
            {
                mail.sendDevs("Unparsable data in dataTransformer.java");
            }

            Integer num = map.get(s.getCustomerID());
            if(num==null)
            {
                map.put(s.getCustomerID(), saleAmount);
            }
            else
            {
                map.put(s.getCustomerID(), saleAmount+num);
            }
        }
        return convert(map);
    }

    public static List<pair<Integer, String>> cash_by_customerID_transform(List<Listable> input)
    {
        Map<String, Integer> map = new TreeMap<String, Integer>();

        for(Listable l : input)
        {
            Sale s = (Sale) l;

            int saleAmount=0;
            int saleItemPrice=0;
            try
            {
                saleAmount = Integer.parseInt(s.getSaleAmount());
                saleItemPrice=Integer.parseInt(s.getSaleItemPrice());
            }
            catch(Exception e)
            {
                mail.sendDevs("Unparsable data in dataTransformer.java");
            }

            Integer priceSoFar = map.get(s.getCustomerID());
            if(priceSoFar==null)
            {
                map.put(s.getCustomerID(), saleAmount*saleItemPrice);
            }
            else
            {
                map.put(s.getCustomerID(), priceSoFar+(saleAmount*saleItemPrice));
            }
        }
        return convert(map);
    }
}
