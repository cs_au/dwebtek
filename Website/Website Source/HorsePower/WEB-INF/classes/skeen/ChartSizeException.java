package skeen;

public class ChartSizeException extends Exception
{
    public ChartSizeException(String s)
    {
        super(s);
    }
}
