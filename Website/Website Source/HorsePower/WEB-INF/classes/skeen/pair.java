package skeen;

public class pair<T1, T2>
{
    private T1 _1;
    private T2 _2;

    public pair(T1 _1, T2 _2)
    {
        this._1 = _1;
        this._2 = _2;
    }

    public int hashCode()
    {
        int hash1 = _1 != null ? _1.hashCode() : 0;
        int hash2 = _2 != null ? _2.hashCode() : 0;

        return ((hash1 + hash2) * hash2) + hash1;
    }

    public boolean equals(Object oth) 
    {
        if(oth instanceof pair) 
        {
            pair op = (pair) oth;
            return
            (
                (this._1 == op._1 ||
                (this._1 != null && 
                 op._1 != null && 
                 this._1.equals(op._1)))

                &&
                
                (this._2 == op._2 ||
                (this._2 != null && 
                 op._2 != null && 
                 this._2.equals(op._2)))
            );
        }
        return false;
    }

    public String toString()
    { 
        return "(" + _1 + ", " + _2 + ")"; 
    }

    public T1 get1() 
    {
        return _1;
    }

    public void set1(T1 _1) 
    {
        this._1 = _1;
    }

    public T2 get2() 
    {
        return _2;
    }

    public void set2(T2 _2) 
    {
        this._2 = _2;
    }
}

