package skeen;

import java.util.Properties;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletContext;

public class mail
{
    private mail()
    {
    }

    private static String login_host = "smtp.gmail.com";
    private static String login_user = "daimler.servlet@gmail.com";
    private static String login_pass = "NINJA-TESTER";

    private static String[] devEmails = null;
    public static void seedServletContext(ServletContext init_con)
    {
        //Seed Dev Mails
		String recv = init_con.getInitParameter("email-devs");
        if(recv!=null)
        {
		    String[] to = recv.split(" ");
            devEmails = to;
        }
    }

    public static boolean sendDevs(String contents)
    {
		return send(devEmails, login_user, "Automated BugReport System Triggered!", contents);
    }

    public static boolean sendDevsContact(String contents)
    {
        return send(devEmails, login_user, "Email from contactform", contents);
    }

    public static boolean send(String to, String from, String subject, String contents)
    {
        String[] strarray = new String[1];
        strarray[0] = to;
        return send(strarray, from, subject, contents);
    }

    public static boolean send(String[] to, String from, String subject, String contents)
    {
        try
        {
            Properties props = System.getProperties();
            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.host", login_host);
            props.put("mail.smtp.user", login_user);
            props.put("mail.smtp.password", login_pass);
            props.put("mail.smtp.port", "587");
            props.put("mail.smtp.auth", "true");

            Session session = Session.getDefaultInstance(props, null);
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(from));

            InternetAddress[] toAddress = new InternetAddress[to.length];

            // To get the array of addresses
            for( int i=0; i < to.length; i++ )
            {
                toAddress[i] = new InternetAddress(to[i]);
            }

            for( int i=0; i < toAddress.length; i++)
            {
                message.addRecipient(Message.RecipientType.TO, toAddress[i]);
            }
            message.setSubject(subject);
            message.setText(contents);
            Transport transport = session.getTransport("smtp");
            transport.connect(login_host, login_user, login_pass);
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
            return true;
        }
        catch(Exception e)
        {
            return false;
        }
    }
}

