package skeen;

import java.util.List;
import org.jdom.Document;

public interface Listable
{
	public String getRequestString();
	public List<Listable> generateMatches(Document doc);
}
