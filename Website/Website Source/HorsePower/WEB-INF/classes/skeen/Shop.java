package skeen;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.Namespace;
import skeen.textEscape;

public class Shop implements Listable
{
	private static Namespace w = Namespace.getNamespace("http://www.cs.au.dk/dWebTek/2011");
	private String shopID, shopName, shopURL;
	
	// Constructor
	public Shop(String shopID, String shopName, String shopURL)
	{
		this.shopID = shopID;
		this.shopName = shopName;
		this.shopURL = shopURL;
	}
	
	// Empty constructor
	public Shop() 
    {
    }
	
	// Returns the request string
	public String getRequestString()
	{
		return "listShops";
	}
	
	// Returns a list of Shop objects
	public List<Listable> generateMatches(Document doc)
	{
		List<Listable> result = new ArrayList<Listable>();
		Iterator iterator = doc.getRootElement().getChildren("shop", w).iterator();
		
		while (iterator.hasNext())
		{
			Element e = (Element)iterator.next();
			String id =   e.getChildText("shopID", w);
			String name = e.getChildText("shopName", w);
			String url =  e.getChildText("shopURL", w);
			
			Shop shop = new Shop(textEscape.forHTML(id),
                                 textEscape.forHTML(name),
                                 textEscape.forHTML(url));
            result.add(shop);
		}
		
		return result;
	}
	
	// Get methods used in JSP
	public String getID() { return shopID; }
	public String getName() { return shopName; }
	public String getURL() { return shopURL; }
}
