import java.io.IOException;
import java.net.URLEncoder;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.jdom.Document;
import skeen.*;

public class ContactServlet extends HttpServlet
{
    public void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException
    {
        HttpSession session = request.getSession();
        String previousPage = request.getParameter("mainPage");
		
		String senderName  = request.getParameter("name");
		String senderEmail = request.getParameter("email");
        String senderMessage = request.getParameter("message");

        if(senderName==null || senderEmail==null || senderMessage==null)
        {
             response.sendRedirect(response.encodeRedirectURL(previousPage + "?contactError=" + URLEncoder.encode("Missing Argument", "UTF-8")));
             return;
        }
        else if(senderMessage.length() < 3)
        {
            response.sendRedirect(response.encodeRedirectURL(previousPage + "?addToCartError=" + URLEncoder.encode("Please supply a longer message", "UTF-8")));
            return;
        }
        
        boolean status = mail.sendDevsContact("From:\t" + senderName + "[" + senderEmail + "]\n" + senderMessage);
        if(!status)
        {
            response.sendRedirect(response.encodeRedirectURL(previousPage + "?addToCartError=" + URLEncoder.encode("Could not send your mail", "UTF-8")));
        }

        response.sendRedirect(response.encodeRedirectURL(previousPage));
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        doPost(request, response);
    }
}

