import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import skeen.*;

public class ServerInit implements ServletContextListener
{
	public void contextInitialized(ServletContextEvent contextEvent)
    {
        ServletContext con = contextEvent.getServletContext();

        Controller.preload(con);

        mail.seedServletContext(con);
        //Sale.seedServletContext(con);
        //Item.seedServletContext(con);
	}
	
    public void contextDestroyed(ServletContextEvent contextEvent)
    {
    }
}

