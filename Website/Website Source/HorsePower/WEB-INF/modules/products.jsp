<!-- !Dep-Bean=Item -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div id="items">
<c:if test="${param.addToCartError != null}"><span class="error">${param.addToCartError}</span><br/><br/></c:if>
<c:forEach items="${items.matches}" var="item">
<hr/>
<table>
	<tr>
		<td class="img"><img src="${item.getURL()}" alt="."/></td>
		<td class="desc">
			<h3>${item.getName()}</h3>
			${item.getPrice()} kr.<br/>
			<span class="small">
				<c:choose>
					<c:when test="${item.getStock() == '0'}">
						Sold out!
					</c:when>
					<c:otherwise>
						(${item.getStock()} stk. p� lager)
					</c:otherwise>
				</c:choose>
			</span><br/>
			<p>${item.getDescription()}</p>
		</td>
		<td class="buy">
			<c:if test="${customerID != null}">
				<form method="post" action="add">
					<input type="hidden" name="mainPage" value="${mainPage}"/>
					<input name="itemID" type="hidden" value="${item.getID()}"/>
					<input name="itemName" type="hidden" value="${item.getName()}"/>
					<input name="itemURL" type="hidden" value="${item.getURL()}"/>
					<input name="itemPrice" type="hidden" value="${item.getPrice()}"/>
					<input name="itemStock" type="hidden" value="${item.getStock()}"/>
					<input name="itemDescription" type="hidden" value="${item.getDescription()}"/>
					<input name="saleAmount" type="text" value="1" style="width: 40px; text-align: right;"/>
					<input type="submit" name="add" value="Add"/>
				</form>
            </c:if>
		</td>
	</tr>
</table>
</c:forEach>
</div>
