<!-- !Dep-Bean=Cart -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<h2>Shopping cart</h2>
<c:choose>
	<c:when test="${cartmodel.matches.isEmpty()}">
		Your shopping cart is empty.<br/>
		&nbsp;
	</c:when>
	<c:otherwise>
		Your shopping cart contains:<br/>
		<div id="cart">
			<c:forEach items="${cartmodel.matches}" var="item">
				<li>${item.getStock()} ${item.getName()}</li>
			</c:forEach>
		</div>
		<!-- Skriv sammen -->
        <form method="post" action="buy">
		    <input type="hidden" name="mainPage" value="${mainPage}"/>
			<input type="submit" name="submit" value="Checkout!"/>
		</form>
        <form method="post" action="clear">
	    	<input type="hidden" name="mainPage" value="${mainPage}"/>
			<input type="submit" name="submit" value="Empty cart"/>
		</form>
	</c:otherwise>
</c:choose>
