<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="j" %>

<c:if test="${customerID == null}">
	<h2>Register a new account</h2>
	<form method="post" action="register">
		<input type="hidden" name="mainPage" value="${mainPage}"/>
		<label>Username: </label><br/>
		<input type="text" name="customerName"/><br/><br/>
		<label>Password: </label><br/>
		<input type="password" name="customerPass"/><br/><br/>
		<label>Password (again): </label><br/>
		<input type="password" name="customerPassRepeat"/><br/><br/>
		<c:if test="${param.registererror != null}"><span class="error">${param.registererror}</span><br/><br/></c:if>
		<input type="submit" name="submit" value="Register"/>
	</form>
</c:if>

<c:choose>
	<c:when test="${param.newaccount != null}">
		<h2>Congratulations!</h2>
		<p>You've just registered a new account. Go <a href="home">buy our stuff</a>!</p>
	</c:when>
	<c:otherwise>
		<c:if test="${customerID != null}">
			<h2>Dude,</h2>
			<p>You're already logged in... <a href="home">Wicked</a>!</p>
		</c:if>
	</c:otherwise>
</c:choose>